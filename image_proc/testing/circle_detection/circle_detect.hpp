#ifndef CIRCLE_DETECT_HPP
#define CIRCLE_DETECT_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

int detect_circle(const cv::Mat&, std::vector<cv::Vec3f>&);

int draw_detected_circles(cv::Mat&, std::vector<cv::Vec3f>&);

#endif