//Circle Detection

#include "circle_detect.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int detect_circle(const cv::Mat& image, std::vector<cv::Vec3f>& circles)
{
	Matjjhjhbj src_gray;

	if( !image.data )
		return -1;
	cvtColor( image, src_gray, CV_BGR2GRAY );
	GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );
	HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 100, 50, 0, 0 );
	return circles.size();
}

int draw_detected_circles(cv::Mat& image, std::vector<cv::Vec3f>& circles)
{
	if(circles.size() == 0)
		return 0;

	for( size_t i = 0; i < circles.size(); i++ )
	{
			Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
			//cout << "center: " << circles[i][0] << "," << circles[i][1] << endl;
			int radius = cvRound(circles[i][2]);
			//cout<<"radius: "<<radius;
			// circle center
			circle( image, center, 3, Scalar(0,255,0), -1, 8, 0 );
			// circle outline
			circle( image, center, radius, Scalar(0,0,255), 3, 8, 0 );
	 }
	 return 1;
}
/** @function main */
/*int main(int argc, char** argv)
{
	Mat src, src_gray;

	/// Read the image
	src = imread( argv[1], 1 );

	if( !src.data )
		{ return -1; }

	/// Convert it to gray
	cvtColor( src, src_gray, CV_BGR2GRAY );

	/// Reduce the noise so we avoid false circle detection
	GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );

	vector<Vec3f> circles;

	/// Apply the Hough Transform to find the circles
	HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 100, 50, 0, 0 );
	cout<<"Circle: "<<circles.size()<<endl;
	/// Draw the circles detected
	for( size_t i = 0; i < circles.size(); i++ )
	{
			Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
			cout << "center: " << circles[i][0] << "," << circles[i][1] << endl;
			int radius = cvRound(circles[i][2]);
			cout<<"radius: "<<radius;
			// circle center
			circle( src, center, 3, Scalar(0,255,0), -1, 8, 0 );
			// circle outline
			circle( src, center, radius, Scalar(0,0,255), 3, 8, 0 );
	 }

	/// Show your results
	namedWindow( "Hough Circle Transform Demo", CV_WINDOW_AUTOSIZE );
	imshow( "Hough Circle Transform Demo", src );

	waitKey(0);
	return 0;
}*/