#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <stdio.h>

#include "circle_detect.hpp"

using namespace cv;
using namespace std;

int main(int argv, char** argc)
{
	cout<<"In"<<endl;
	src = imread( argv[1], 1 );

	if( !src.data )
		{ return -1; }

	namedWindow("test", CV_WINDOW_AUTOSIZE);

	vector<Vec3f> circles;

	if(detect_circle(src, circles) > 0)
		if(draw_detected_circles(src, circles) > 0)
		{
			imshow("test", src);
		}
	waitKey(0);
	return 0;
}