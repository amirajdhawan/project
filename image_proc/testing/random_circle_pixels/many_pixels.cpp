#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <stdlib.h>

using namespace cv;
using namespace std;

void rand_pixels_rectangle(Mat& , vector<Point>& , vector<Point> , int);
void rand_pixels_circle(Mat&, vector<Point>&, Point, int, int);

int main(int argc, char** argv)
{
	Mat src = Mat(500,500,CV_8UC3);
	
	vector<Point> random_test_points;
	vector<Point> sqaure;
	sqaure.push_back(Point(200,200));
	sqaure.push_back(Point(300,300));
	sqaure.push_back(Point(200,400));
	sqaure.push_back(Point(100,300));
	

	#if 1
	//rectangle(src, sqaure[0], sqaure[2],  Scalar(0, 255, 0));
	line(src, sqaure[0],sqaure[1],Scalar(0, 255, 0));
	line(src, sqaure[1],sqaure[2],Scalar(0, 255, 0));
	line(src, sqaure[2],sqaure[3],Scalar(0, 255, 0));
	line(src, sqaure[3],sqaure[0],Scalar(0, 255, 0));
	rand_pixels_rectangle(src, random_test_points, sqaure, 20);

	#else
	cout<<"Enter Radius : ";
	int radius;
	cin>>radius;
	Point center;
	cout<<"Enter x coordinate of pixel : ";
	cin>>center.x;
	cout<<"Enter y coordinate of pixel : ";
	cin>>center.y;
	
	circle(src, center, radius, Scalar(0,0,255));
	rand_pixels_circle(src, random_test_points, center, radius, 30);
	#endif
	for(int i = 0; i < random_test_points.size(); i++){
		cout<<"Point 1: "<<random_test_points[i].x<<","<<random_test_points[i].y<<endl; 
	}
	namedWindow("temp",1);
	
	imshow("temp",src);
	waitKey(0);
	return 0;
}

void rand_pixels_rectangle(Mat& src, vector<Point>& random_test_points, vector<Point> sqaure, int no_of_rand_points ){
	
	int upper_bound = 0, lower_bound = 100000, right_bound = 0, left_bound = 100000;
 	
 	for(int i = 0; i < sqaure.size(); i++){
 		if(sqaure[i].x > right_bound)
 			right_bound = sqaure[i].x;
 		if(sqaure[i].x < left_bound)
 			left_bound = sqaure[i].x;

 		if(sqaure[i].y > upper_bound)
 			upper_bound = sqaure[i].y;
 		if(sqaure[i].y < lower_bound)
 			lower_bound = sqaure[i].y;
 	}
 	
 	int x_range = abs(right_bound - left_bound);
	int y_range = abs(upper_bound - lower_bound);
	cout<<"lower_bound: "<<lower_bound<<" upper_bound: "<<upper_bound<<" right_bound:"<<right_bound<<" left_bound: "<<left_bound<<endl;
 	//vector<Point> approx_curve;
 	Point random_point;
 	//approxPolyDP(sqaure, approx_curve, )
 	int i = 0;
 	rectangle(src, Point(left_bound, upper_bound), Point(right_bound, lower_bound), Scalar(0, 255, 0));
	
 	while(i < no_of_rand_points){
 		cout<<"in";
 		random_point.x = rand() % x_range + left_bound;
 		random_point.y = rand() % y_range + lower_bound;
 		double test = pointPolygonTest(sqaure, random_point, false);
 		if(test >= 0){
 			random_test_points.push_back(random_point);
 			i++;
 			circle(src, random_point, 1, Scalar(255,0,0));
 		}
 	}
}

void rand_pixels_circle(Mat& src, vector<Point>& random_test_points, Point center, int radius, int no_of_rand_points){
	int upper_bound = 0, lower_bound = 0, right_bound = 0, left_bound = 0;

	upper_bound = center.y + int(radius);
	lower_bound = center.y - int(radius);
	right_bound = center.x + int(radius);
	left_bound  = center.x - int(radius);

	Point random_point;
	int x_range = abs(right_bound - left_bound);
	int y_range = abs(upper_bound - lower_bound);

	int r_square = pow(radius,2);
	
	rectangle(src, Point(left_bound, upper_bound), Point(right_bound, lower_bound), Scalar(0, 255, 0));
	
	int i = 0;
	while(i < no_of_rand_points)
	{
		random_point.x = rand() % x_range + left_bound;
		random_point.y = rand() % y_range + lower_bound;
		int equation_circle = (pow((random_point.x - center.x), 2) + pow((random_point.y - center.y), 2));
		if(equation_circle < r_square){
			circle(src, random_point, 1, Scalar(255,0,0));	
			i++;
			random_test_points.push_back(random_point);
		}
		
		//cout<<random_point.x<<","<<random_point.y<<endl;	
	}
}