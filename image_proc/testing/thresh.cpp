#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui.hpp"

Mat GetThresholdedImage(Mat& img)
{
	// Convert the image into an HSV image
	Mat imgHSV;
	cvCvtColor(img, imgHSV, CV_BGR2HSV);
	Mat imgThreshed;	// = cvCreateImage(cvGetSize(img), 8, 1);
	cvInRangeS(imgHSV, cvScalar(0, 0, 200), cvScalar(8, 8, 255), imgThreshed);
//	cvReleaseImage(&imgHSV);
    	return imgThreshed;
}

int main(int argc, char* argv[])
{
	cvNamedWindow("video");
	cvNamedWindow("thresh");
	Mat input = imread(argv[1]);
	Mat imgYellowThresh = GetThresholdedImage(input);
	cvShowImage("output", imgYellowThresh);
	cvShowImage("input", input);
	cvReleaseImage(&imgYellowThresh);
	cvReleaseCapture(&capture);
	return 0;
}
