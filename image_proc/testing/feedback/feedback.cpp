//Feedback
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <SerialStream.h>

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	VideoCapture cam(1);

	namedWindow("output");
	namedWindow("input");
	Mat image_HSV, img_Threshed;
	Moments mom;

	Mat image;
	while(1){
	cam>> image;
	
	cvtColor(image, image_HSV, CV_BGR2HSV);
	cout<<"Color convert";
	//inRange(image_HSV, Scalar(150, 20, 0), Scalar(255, 120, 80), img_Threshed);
	inRange(image_HSV, Scalar(20, 80, 80), Scalar(30, 255, 255), img_Threshed);
	//cout<<"Color convert";
	mom = moments(img_Threshed, true);
	
	int xcenter = 0, ycenter = 0;
	xcenter = mom.m10 / mom.m00;
	ycenter = mom.m01 / mom.m00;
	
	Point temp;
	temp.x = xcenter;
	temp.y = ycenter;
	
	circle(img_Threshed, temp, 25, Scalar(155,155,255), 2, 8);

	imshow("input", image);
	imshow("output", img_Threshed);
	waitKey(100);
}
	waitKey(0);

	return 0;
}