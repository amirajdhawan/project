#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
	VideoCapture cap(0); 
	namedWindow("temp",1);
    if(!cap.isOpened())
    {
    	cout<<"No capture"<<endl;
    	return -1;
    }
    while(1){
    	Mat frame;
    	cap >> frame;
    	// if(frame.empty())
    	imshow("temp",frame);
    	if(waitKey(10) >= 0) 
    		break;
    }
    cap.release();
    waitKey(0);
    return 0;
}