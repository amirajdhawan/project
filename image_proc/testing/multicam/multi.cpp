#include <iostream>
    #include <opencv2/highgui/highgui.hpp>

    int main()

    {
    cv::VideoCapture cap1, cap2;
    cv::Mat frame1, image1, frame2, image2;

    cap1.open(0);
    cap2.open(1);
    if( !cap1.isOpened() )
    {
        std::cout << "Could not initialize cap1"<<std::endl;
        return -1;
    }
    if( !cap2.isOpened() )
    {
        std::cout << "Could not initialize cap2"<<std::endl;
        return -1;
    }
    for(;;)
    {
        cap1 >> frame1;
        if( frame1.empty() )
            break;
        frame1.copyTo(image1);
        cap2 >> frame2;
        if( frame2.empty() )
            break;
        frame2.copyTo(image2);
        cv::imshow( "Img1", image1 );
        cv::imshow( "Img2", image2 );

        char c = (char)cv::waitKey(10);
        if( c == 27 )
            break;
    }

    return 1;
}