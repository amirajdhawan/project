project( hand_gesture_detection )
find_package( OpenCV REQUIRED )
add_executable( hand_gesture_detection hand_gesture_detection )
target_link_libraries( hand_gesture_detection ${OpenCV_LIBS} )

