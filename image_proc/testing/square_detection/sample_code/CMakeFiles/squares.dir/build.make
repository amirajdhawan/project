# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/cmake-gui

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/amiraj/project/image_proc/square_detection

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/amiraj/project/image_proc/square_detection

# Include any dependencies generated for this target.
include CMakeFiles/squares.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/squares.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/squares.dir/flags.make

CMakeFiles/squares.dir/squares.o: CMakeFiles/squares.dir/flags.make
CMakeFiles/squares.dir/squares.o: squares.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/square_detection/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/squares.dir/squares.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/squares.dir/squares.o -c /home/amiraj/project/image_proc/square_detection/squares.cpp

CMakeFiles/squares.dir/squares.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/squares.dir/squares.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/square_detection/squares.cpp > CMakeFiles/squares.dir/squares.i

CMakeFiles/squares.dir/squares.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/squares.dir/squares.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/square_detection/squares.cpp -o CMakeFiles/squares.dir/squares.s

CMakeFiles/squares.dir/squares.o.requires:
.PHONY : CMakeFiles/squares.dir/squares.o.requires

CMakeFiles/squares.dir/squares.o.provides: CMakeFiles/squares.dir/squares.o.requires
	$(MAKE) -f CMakeFiles/squares.dir/build.make CMakeFiles/squares.dir/squares.o.provides.build
.PHONY : CMakeFiles/squares.dir/squares.o.provides

CMakeFiles/squares.dir/squares.o.provides.build: CMakeFiles/squares.dir/squares.o

# Object files for target squares
squares_OBJECTS = \
"CMakeFiles/squares.dir/squares.o"

# External object files for target squares
squares_EXTERNAL_OBJECTS =

squares: CMakeFiles/squares.dir/squares.o
squares: /usr/local/lib/libopencv_calib3d.so
squares: /usr/local/lib/libopencv_contrib.so
squares: /usr/local/lib/libopencv_core.so
squares: /usr/local/lib/libopencv_features2d.so
squares: /usr/local/lib/libopencv_flann.so
squares: /usr/local/lib/libopencv_gpu.so
squares: /usr/local/lib/libopencv_highgui.so
squares: /usr/local/lib/libopencv_imgproc.so
squares: /usr/local/lib/libopencv_legacy.so
squares: /usr/local/lib/libopencv_ml.so
squares: /usr/local/lib/libopencv_nonfree.so
squares: /usr/local/lib/libopencv_objdetect.so
squares: /usr/local/lib/libopencv_photo.so
squares: /usr/local/lib/libopencv_stitching.so
squares: /usr/local/lib/libopencv_ts.so
squares: /usr/local/lib/libopencv_video.so
squares: /usr/local/lib/libopencv_videostab.so
squares: CMakeFiles/squares.dir/build.make
squares: CMakeFiles/squares.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable squares"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/squares.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/squares.dir/build: squares
.PHONY : CMakeFiles/squares.dir/build

CMakeFiles/squares.dir/requires: CMakeFiles/squares.dir/squares.o.requires
.PHONY : CMakeFiles/squares.dir/requires

CMakeFiles/squares.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/squares.dir/cmake_clean.cmake
.PHONY : CMakeFiles/squares.dir/clean

CMakeFiles/squares.dir/depend:
	cd /home/amiraj/project/image_proc/square_detection && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/amiraj/project/image_proc/square_detection /home/amiraj/project/image_proc/square_detection /home/amiraj/project/image_proc/square_detection /home/amiraj/project/image_proc/square_detection /home/amiraj/project/image_proc/square_detection/CMakeFiles/squares.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/squares.dir/depend

