#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;
using namespace cv;

RNG rng(12345);

vector<Point> frame;

static double angle( Point pt1, Point pt2, Point pt0 )
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

void find_rects(Mat& image, vector<vector<Point> >& rects, vector<vector<Point> >& triangles)
{
	rects.clear();
	Mat pyr, timg, gray, bw;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	pyrDown(image, pyr, Size(image.cols/2, image.rows/2));
    pyrUp(pyr, timg, image.size());
    cvtColor(timg, gray, CV_RGB2GRAY);
    //bw = gray > 128;
    //imshow("sqaure",gray);
    
    threshold( gray, bw, 220, 255, THRESH_BINARY ); 
    imshow("Thresholded Image",bw);
    findContours( bw, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
   	cout<<"Contours Size: "<<contours.size();

   	Mat drawing = Mat::zeros( bw.size(), CV_8UC3 );

   	for( int i = 0; i < contours.size(); i++ )
	{
		Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
		drawContours( drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
		//drawContours( drawing, hull, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
	}
	//imshow("sqaure",drawing);

	int max_area_index = 0;
	double max_area = 0;
	//int size = contours.size();
	double *contours_area;
	
	contours_area = (double *)malloc(sizeof(double)*contours.size());

	for( int i = 0; i < contours.size(); i++)
	{
		contours_area[i] = contourArea(contours[i]);
		cout<<endl<<i<<" area = "<<contours_area[i];
		if(contours_area[i] > max_area)
		{
			max_area = contours_area[i];
			max_area_index = i;
		}
	}
	//contours.erase(max_area_index);
	//cout<<endl<<endl<<"Max area: "<<max_area<<" at index = "<<max_area_index<<endl<<endl;
	//cout<<"Contours Size: "<<contours.size()<<endl<<"Contour 1 x,y: "<<contours[0][0].x<<"  "<<contours[0][0].y;
	vector<Point> approx_square;

	for( int i = 0; i < contours.size(); i++)
	{
		if(i == max_area_index)
			continue;
		approxPolyDP(Mat(contours[i]), approx_square, arcLength(Mat(contours[i]), true)*0.02, true);
		if(approx_square.size() == 4 && contours_area[i] > 1000 && isContourConvex(Mat(approx_square)))
		{
			double maxCosine = 0;
			for( int j = 2; j < 5; j++ )
			{
			    // find the maximum cosine of the angle between joint edges
			    double cosine = fabs(angle(approx_square[j%4], approx_square[j-2], approx_square[j-1]));
			    maxCosine = MAX(maxCosine, cosine);
			}
			if( maxCosine < 0.3 )
				rects.push_back(approx_square);
		}
		#if 1
		else{
			if(approx_square.size() == 3 && contours_area[i] > 1000 && isContourConvex(Mat(approx_square)))
			{
				//cout<<endl<<"inside triangle"<<endl;
				double angle_total = 0;
				for( int j = 0; j < 3; j++ )
				{
				    double cosine = fabs(angle(approx_square[(j+1)%3], approx_square[(j+2)%3], approx_square[j]));
				    angle_total += acos(cosine);
				}
				//cout<<"angle: "<<angle_total<<endl;
				if(angle_total <  3.40339204 && angle_total > 2.87979327)
					triangles.push_back(approx_square);
			}
		}
		#endif
	}
}

/*void display_rects(vector<vector<Point> >& rects)
{
	for( int i = 0; i < rects.size(); i++)
	{
		cout<<endl<<"Index: "<<i<<"  square "
	}
}
*/

void drawrects( Mat& image, const vector<vector<Point> >& rects, const vector<vector<Point> >& triangles )
{
    Scalar colr = Scalar(0,0,255);
    
    cout<<endl<<"Rect Size: "<<rects.size()<<endl;
    cout<<endl<<"Tri Size: "<<triangles.size()<<endl;
    for( size_t i = 0; i < rects.size(); i++ )
    {
        const Point* p = &rects[i][0];
        int n = (int)rects[i].size();
        circle(image, rects[i][0], 5, colr);

        polylines(image, &p, &n, 1, true, Scalar(0,255,0), 3, CV_AA);
    }
    for( size_t i = 0; i < triangles.size(); i++ )
    {
        const Point* p = &triangles[i][0];
        int n = (int)triangles[i].size();
        circle(image, triangles[i][0], 5, colr);

        polylines(image, &p, &n, 1, true, Scalar(0,255,0), 3, CV_AA);
    }
    imshow("Rectangles", image);
}

int main(int argc, char** argv)
{
	namedWindow("Rectangles",1);
	namedWindow("Thresholded Image",1);
	vector<vector<Point> > rects;
	vector<vector<Point> > triangles;

	Mat image = imread(argv[1], 1);
	Mat final = image;
	if( image.empty() )
	{
	    cout << "Couldn't load Image: " << argv[1] << endl;
	}

	find_rects(image, rects, triangles);
	
	drawrects(final, rects, triangles);

	int c = waitKey();
	if( (char)c == 27 )
		return 0;
}