#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

int thresh = 0;
RNG rng(12345);
Mat threshold_output;
vector<vector<Point> > contours;
vector<Vec4i> hierarchy;

Mat src;
vector<Vec4i> defects;
vector<Point> hull;
vector<int>  hullsI;
vector<Point> largest_contour;

void find_contour(int, void*);

int main( int argc, char** argv )
{
	VideoCapture cap(0);
	if(!cap.isOpened())
	{
		cout<<"\nUnable to connect to capture device"<<endl;
		return -1;
	}

	namedWindow( "Hull demo", CV_WINDOW_AUTOSIZE );
	namedWindow( "source", CV_WINDOW_AUTOSIZE );
	namedWindow( "thres", CV_WINDOW_AUTOSIZE );
	namedWindow( "gray", CV_WINDOW_AUTOSIZE );
	createTrackbar("Threshold: ", "Hull demo", &thresh, 255, &find_contour);

	while(1){
		cap >> src;
		find_contour(0,0);

		waitKey(100);

	}
	return 0;
}

void find_contour(int, void*)
{
	//Mat src_copy = src.clone();
	Mat src_gray;

	imshow("source", src);

	cvtColor( src, src_gray, CV_BGR2GRAY );
	blur( src_gray, src_gray, Size(3,3) );
	imshow("gray",src_gray);
	//do{
		threshold( src_gray, threshold_output, thresh, 255, THRESH_OTSU );
		//adaptiveThreshold( src_gray, threshold_output, 255, ADAPTIVE_THRESH_GAUSSIAN_C,  THRESH_BINARY, 3, -1 );
		//bitwise_not(threshold_output,threshold_output);
		imshow("thres",threshold_output);
			
		findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
	//	thresh++;
	//}while(thresh < 200);

	//cout<<"\nthresh:"<<thresh;
	
	int max_area = 0;
	int index = 0;
	double current_area;

	for(int i = 0; i < contours.size(); i++){
		
		current_area = contourArea(contours[i]);
		if(current_area > max_area){
			max_area = current_area;
		 	index = i;
		}
	}
	
	largest_contour = contours[index];
	//free(contours);
	
	convexHull( Mat(largest_contour), hull, false );
	convexHull( Mat(largest_contour), hullsI, false );
	
	if(hullsI.size() > 2 && largest_contour.size() > 2 )
	{
		try{
			convexityDefects(largest_contour, hullsI, defects);
		}
		catch(cv::Exception& e){
			//thresh--;
			//max_thresh = thresh;
			return;
		}
	}

	int min_y = 1000000;
	Point top_most_point;
	index = 0;

	for(int k = 0; k < hull.size();k++){
		if(hull[k].y < min_y){
			min_y = hull[k].y;
			top_most_point = hull[k];
			index = k;
		}
	}

	Mat drawing = Mat::zeros( threshold_output.size(), CV_8UC3 );
		
	const Point* p = &largest_contour[0];
	int n = (int)largest_contour.size();
	polylines(drawing, &p, &n, 1, true, Scalar(0,0,255), 3, CV_AA);
	int temp = 0;

	for(int i = 0; i < defects.size(); i++){
		temp = defects[i][0];
		if((largest_contour[temp].x == top_most_point.x) && (largest_contour[temp].y == top_most_point.y))
    		circle(drawing, top_most_point, 5, Scalar(0,255,0), -1, 8, 0);
	}

	Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
	vector<vector<Point> > tempd;
	vector<vector<Point> > hulltemp;
	tempd.push_back(largest_contour);
	hulltemp.push_back(hull);

	drawContours( drawing, tempd, 0, color, 1, 8, vector<Vec4i>(), 0, Point() );
	//drawContours( drawing, hulltemp, 0, color, 1, 8, vector<Vec4i>(), 0, Point() );	
	imshow( "Hull demo", drawing );
}













