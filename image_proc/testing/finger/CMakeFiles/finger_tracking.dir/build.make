# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/cmake-gui

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/amiraj/project/image_proc/testing/finger

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/amiraj/project/image_proc/testing/finger

# Include any dependencies generated for this target.
include CMakeFiles/finger_tracking.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/finger_tracking.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/finger_tracking.dir/flags.make

CMakeFiles/finger_tracking.dir/finger_tracking.o: CMakeFiles/finger_tracking.dir/flags.make
CMakeFiles/finger_tracking.dir/finger_tracking.o: finger_tracking.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/testing/finger/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/finger_tracking.dir/finger_tracking.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/finger_tracking.dir/finger_tracking.o -c /home/amiraj/project/image_proc/testing/finger/finger_tracking.cpp

CMakeFiles/finger_tracking.dir/finger_tracking.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/finger_tracking.dir/finger_tracking.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/testing/finger/finger_tracking.cpp > CMakeFiles/finger_tracking.dir/finger_tracking.i

CMakeFiles/finger_tracking.dir/finger_tracking.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/finger_tracking.dir/finger_tracking.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/testing/finger/finger_tracking.cpp -o CMakeFiles/finger_tracking.dir/finger_tracking.s

CMakeFiles/finger_tracking.dir/finger_tracking.o.requires:
.PHONY : CMakeFiles/finger_tracking.dir/finger_tracking.o.requires

CMakeFiles/finger_tracking.dir/finger_tracking.o.provides: CMakeFiles/finger_tracking.dir/finger_tracking.o.requires
	$(MAKE) -f CMakeFiles/finger_tracking.dir/build.make CMakeFiles/finger_tracking.dir/finger_tracking.o.provides.build
.PHONY : CMakeFiles/finger_tracking.dir/finger_tracking.o.provides

CMakeFiles/finger_tracking.dir/finger_tracking.o.provides.build: CMakeFiles/finger_tracking.dir/finger_tracking.o

# Object files for target finger_tracking
finger_tracking_OBJECTS = \
"CMakeFiles/finger_tracking.dir/finger_tracking.o"

# External object files for target finger_tracking
finger_tracking_EXTERNAL_OBJECTS =

finger_tracking: CMakeFiles/finger_tracking.dir/finger_tracking.o
finger_tracking: /usr/local/lib/libopencv_calib3d.so
finger_tracking: /usr/local/lib/libopencv_contrib.so
finger_tracking: /usr/local/lib/libopencv_core.so
finger_tracking: /usr/local/lib/libopencv_features2d.so
finger_tracking: /usr/local/lib/libopencv_flann.so
finger_tracking: /usr/local/lib/libopencv_gpu.so
finger_tracking: /usr/local/lib/libopencv_highgui.so
finger_tracking: /usr/local/lib/libopencv_imgproc.so
finger_tracking: /usr/local/lib/libopencv_legacy.so
finger_tracking: /usr/local/lib/libopencv_ml.so
finger_tracking: /usr/local/lib/libopencv_nonfree.so
finger_tracking: /usr/local/lib/libopencv_objdetect.so
finger_tracking: /usr/local/lib/libopencv_photo.so
finger_tracking: /usr/local/lib/libopencv_stitching.so
finger_tracking: /usr/local/lib/libopencv_ts.so
finger_tracking: /usr/local/lib/libopencv_video.so
finger_tracking: /usr/local/lib/libopencv_videostab.so
finger_tracking: CMakeFiles/finger_tracking.dir/build.make
finger_tracking: CMakeFiles/finger_tracking.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable finger_tracking"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/finger_tracking.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/finger_tracking.dir/build: finger_tracking
.PHONY : CMakeFiles/finger_tracking.dir/build

CMakeFiles/finger_tracking.dir/requires: CMakeFiles/finger_tracking.dir/finger_tracking.o.requires
.PHONY : CMakeFiles/finger_tracking.dir/requires

CMakeFiles/finger_tracking.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/finger_tracking.dir/cmake_clean.cmake
.PHONY : CMakeFiles/finger_tracking.dir/clean

CMakeFiles/finger_tracking.dir/depend:
	cd /home/amiraj/project/image_proc/testing/finger && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/amiraj/project/image_proc/testing/finger /home/amiraj/project/image_proc/testing/finger /home/amiraj/project/image_proc/testing/finger /home/amiraj/project/image_proc/testing/finger /home/amiraj/project/image_proc/testing/finger/CMakeFiles/finger_tracking.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/finger_tracking.dir/depend

