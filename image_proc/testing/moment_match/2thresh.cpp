#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	Mat image;
	Mat image_HSV, img_Threshed_Yellow, img_Threshed_Red,dst;
	Moments mom_y;
	Moments mom_r;
	VideoCapture cap(0); // open the default camera
	if(!cap.isOpened())  // check if we succeeded
	    return -1;

		
	while(1)
	{
		cap >> image;

		cvtColor(image, image_HSV, CV_BGR2HSV);
		cout<<"Color convert";
		//inRange(image_HSV, Scalar(150, 20, 0), Scalar(255, 120, 80), img_Threshed);
		inRange(image_HSV, Scalar(20, 80, 80), Scalar(30, 255, 255), img_Threshed_Yellow);
		inRange(image_HSV, Scalar(0, 80, 80), Scalar(10, 255, 255), img_Threshed_Red);
		//cout<<"Color convert";
		mom_y = moments(img_Threshed_Yellow, true);
		mom_r = moments(img_Threshed_Red, true);
		
		int xcenter_y = 0, ycenter_y = 0, xcenter_r = 0, ycenter_r = 0;
		xcenter_y = mom_y.m10 / mom_y.m00;
		ycenter_y = mom_y.m01 / mom_y.m00;
		
		xcenter_r = mom_r.m10 / mom_r.m00;
		ycenter_r = mom_r.m01 / mom_r.m00;


		float distance = (ycenter_r - ycenter_y)/(xcenter_r - xcenter_y);

		if(distance <20)
		{
			cout<<"Near";
		}

		else
		{
			cout<<"Far";	
		}

		addWeighted( img_Threshed_Yellow, 0.5, img_Threshed_Red, 0.5, 0.0, dst);

		namedWindow("output");
		namedWindow("input");

		imshow("input", image);
		imshow("output", dst);
		int c = cvWaitKey(10);
		if((char)c==27 ) break;
	}

	return 0;
}