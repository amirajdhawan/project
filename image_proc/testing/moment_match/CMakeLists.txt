project( 2thresh )
find_package( OpenCV REQUIRED )
add_executable( 2thresh 2thresh )
target_link_libraries( 2thresh ${OpenCV_LIBS} )
