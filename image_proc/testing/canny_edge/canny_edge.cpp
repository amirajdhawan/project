#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

// Define the IplImage pointers we're going to Suse as globals
IplImage* pFrame;
IplImage* pProcessedFrame;
IplImage* tempFrame;

RNG rng(12345);
 
// Slider for the low threshold value of our edge detection
int maxLowThreshold = 1024;
int lowSliderPosition = 150;
 
// Slider for the high threshold value of our edge detection
int maxHighThreshold = 1024;
int highSliderPosition = 250;
 
// Function to find the edges of a given IplImage object
IplImage* findEdges(IplImage* sourceFrame, double thelowThreshold, double theHighThreshold, double theAperture)
{
	// Convert source frame to greyscale version (tempFrame has already been initialised to use greyscale colour settings)
	cvCvtColor(sourceFrame, tempFrame, CV_RGB2GRAY);
 
	// Perform canny edge finding on tempframe, and push the result back into itself!
	cvCanny(tempFrame, tempFrame, thelowThreshold, theHighThreshold, theAperture);
 
	// Pass back our now processed frame!
	return tempFrame;
}
 
// Callback function to adjust the low threshold on slider movement
void onLowThresholdSlide(int theSliderValue)
{
	lowSliderPosition = theSliderValue;
}

// Callback function to adjust the high threshold on slider movement
void onHighThresholdSlide(int theSliderValue)
{
	highSliderPosition = theSliderValue;
}
 
int main(int argc, char** argv)
{
	// Create two windows
	cvNamedWindow("WebCam", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("Processed WebCam", CV_WINDOW_AUTOSIZE);
 	//cvNamedWindow("Hough Output",CV_WINDOW_AUTOSIZE);

	// Create the low threshold slider
	// Format: Slider name, window name, reference to variable for slider, max value of slider, callback function
	cvCreateTrackbar("Low Threshold", "Processed WebCam", &lowSliderPosition, maxLowThreshold, onLowThresholdSlide);
 
	// Create the high threshold slider
	cvCreateTrackbar("High Threshold", "Processed WebCam", &highSliderPosition, maxHighThreshold, onHighThresholdSlide);
 
	// Create CvCapture object to grab data from the webcam
	CvCapture* pCapture;
 
	// Start capturing data from the webcam
	pCapture = cvCaptureFromCAM(CV_CAP_V4L2);
 
	// Display image properties
	cout << "Width of frame: " << cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_WIDTH) << endl; 		// Width of the frames in the video stream
	cout << "Height of frame: " << cvGetCaptureProperty(pCapture, CV_CAP_PROP_FRAME_HEIGHT) << endl; 	// Height of the frames in the video stream
	cout << "Image brightness: " << cvGetCaptureProperty(pCapture, CV_CAP_PROP_BRIGHTNESS) << endl; 	// Brightness of the image (only for cameras)
	cout << "Image contrast: " << cvGetCaptureProperty(pCapture, CV_CAP_PROP_CONTRAST) << endl; 		// Contrast of the image (only for cameras)
	cout << "Image saturation: " << cvGetCaptureProperty(pCapture, CV_CAP_PROP_SATURATION) << endl;		// Saturation of the image (only for cameras)
	cout << "Image hue: " << cvGetCaptureProperty(pCapture, CV_CAP_PROP_HUE) << endl;			// Hue of the image (only for cameras)
 
	// Create an image from the frame capture
	pFrame = cvQueryFrame(pCapture);
 
	// Create a greyscale image which is the size of our captured image
	pProcessedFrame = cvCreateImage(cvSize(pFrame->width, pFrame->height), IPL_DEPTH_8U, 1);
 
	// Create a frame to use as our temporary copy of the current frame but in grayscale mode
	tempFrame = cvCreateImage(cvSize(pFrame->width, pFrame->height), IPL_DEPTH_8U, 1);
 
	// Loop controling vars
	char keypress;
	bool quit = false;
 
	while (quit == false)
	{
		// Make an image from the raw capture data
		// Note: cvQueryFrame is a combination of cvGrabFrame and cvRetrieveFrame
		pFrame = cvQueryFrame(pCapture);
 
		// Draw the original frame in our window
		cvShowImage("WebCam", pFrame);
 
		// Process the grame to find the edges
		pProcessedFrame = findEdges(pFrame, lowSliderPosition, highSliderPosition, 3);
 		
 		// Showed the processed output in our other window
		cvShowImage("Processed WebCam", pProcessedFrame);
 
		//My Code
		Mat aftercanny(pProcessedFrame);
		//Mat dstmat;
		//Mat firstmat;
		vector<vector<Point> > contours;
  		vector<Vec4i> hierarchy;
		int defects=0;
		findContours( aftercanny, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
		
		vector<vector<Point > >hull (contours.size());
   		
   		//vector<Vec4i>defects;
		for( int i = 0; i < contours.size(); i++ )
      	{  
      		convexHull( Mat(contours[i]), hull[i], false, false );
      		//if(hull[i] > 4)
      		//	defects++;
      		//if(contours[i].size() > 3 )
        	//convexityDefects(contours[i], hull[i], defects[i]);
      	}
		
		Mat drawing = Mat::zeros( aftercanny.size(), CV_8UC3 );
		//cvtColor( aftercanny, dstmat, CV_GRAY2BGR );
		//cvtColor( dstmat, aftercanny, CV_GRAY2BGR);
		/*CvSeq* contourPoints;
		CvSeq* hullPoints;
		CvSeq* defects1;
		CvMemStorage* storage;
		CvMemStorage* strDefects;
		CvMemStorage* contourStr;
		CvMemStorage* hullStr;
		CvConvexityDefect *defectArray = 0;

		strDefects = cvCreateMemStorage();
		defects1 = cvCreateSeq( CV_SEQ_KIND_GENERIC|CV_32SC2, sizeof(CvSeq),sizeof(CvPoint), strDefects );

		//We start converting vector<Point> resulting from findContours
		contourStr = cvCreateMemStorage();
		contourPoints = cvCreateSeq(CV_SEQ_KIND_GENERIC|CV_32SC2, sizeof(CvSeq), sizeof(CvPoint), contourStr);
		//printf("	Metiendo valores\n");
		for(int i=0; i<(int)contours.size(); i++) {
    		CvPoint cp = {contours[i].x,  contours[i].y};
    		cvSeqPush(contourPoints, &cp);
		}
		//Now, the hull points obtained from convexHull c++
		hullStr = cvCreateMemStorage(0);
		hullPoints = cvCreateSeq(CV_SEQ_KIND_GENERIC|CV_32SC2, sizeof(CvSeq), sizeof(CvPoint), hullStr);
		for(int i=0; i<(int)hull.size(); i++) {
    		CvPoint cp = {hull[i].x,  hull[i].y};
    		cvSeqPush(hullPoints, &cp);
		}

		//And we compute convexity defects
		storage = cvCreateMemStorage(0);
		defects1 = cvConvexityDefects(contourPoints, hullPoints, storage);
*/
		cout<<endl<<"Contour size"<<contours.size()<<"   Defects"<<defects;
		
		for( size_t i = 0; i< contours.size(); i++ )
    	{
       		//convexityDefects(contours[i],hull[i],defects[i]);
			Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
       		drawContours( drawing, contours, (int)i, color, 2, 8, hierarchy, 0, Point() );
       		drawContours( drawing, hull, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
     	}

		/*HoughLines(aftercanny, lines, 1, CV_PI/180, 100 );
		for( size_t i = 0; i < lines.size(); i++ )
    	{
        	float rho = lines[i][0];
        	float theta = lines[i][1];
        	double a = cos(theta), b = sin(theta);
        	double x0 = a*rho, y0 = b*rho;
        	Point pt1 (cvRound(x0 + 1000*(-b)), cvRound(y0 + 1000*(a)));
        	Point pt2 (cvRound(x0 - 1000*(-b)), cvRound(y0 - 1000*(a)));
        	line(dstmat, pt1, pt2, Scalar(0,0,255), 3, 8 );
    	}*/
    	//IplImage *opcur(dstmat);
		imshow("Find Contours", drawing);

		// Wait 20 milliseconds
		keypress = cvWaitKey(500);
 
		// Set the flag to quit if escape was pressed
		if (keypress == 27)
		{
			quit = true;
		}
 
	} // End of while loop
 
	// Release our stream capture object to free up any resources it has been using and release any file/device handles
	cvReleaseCapture(&pCapture);
 
	// Release our images
	cvReleaseImage(&pFrame);
	cvReleaseImage(&pProcessedFrame);
	// This causes errors if you don't set it to NULL before releasing it. Maybe because we assign
	// it to pProcessedFrame as the end result of the findEdges function, and we've already released pProcessedFrame!!
	tempFrame = NULL;
	cvReleaseImage(&tempFrame);
 
	// Destory all windows
	cvDestroyAllWindows();
}
