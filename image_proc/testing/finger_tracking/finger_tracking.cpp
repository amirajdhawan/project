//Finger Counting using Contour detection
//With dynamic threshold checking

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

Mat src;
Mat src_gray;
int thresh = 40;
int max_thresh = 200;	//Check different values
RNG rng(12345);

/// Function header
//void thresh_callback(int, void* );
/** @function main */
void find_contour(int, void* );

int main( int argc, char** argv )
{
	/// Load source image and convert it to gray
	VideoCapture cap(0);
	if(!cap.isOpened())
	{
		cout<<"\nUnable to connect to capture device"<<endl;
		return -1;
	}
	/// Create Window
	namedWindow( "source", CV_WINDOW_AUTOSIZE );
	namedWindow( "Hull demo", CV_WINDOW_AUTOSIZE );
	namedWindow( "thres", CV_WINDOW_AUTOSIZE );
	
	/// Convert image to gray and blur it
	
	while(1){
		cap >> src; 
		//src=imread(argv[1],1);
		imshow( "source", src );
	
		cvtColor( src, src_gray, CV_BGR2GRAY );
		blur( src_gray, src_gray, Size(3,3) );
		
		find_contour(0, 0);
		//createTrackbar( " Threshold:", "Hull demo", &thresh, max_thresh, find_contour );
		waitKey(50);
	}
	waitKey(0);
	return(0);
}
/** @function thresh_callback */
void find_contour(int, void* )
{
	Mat src_copy = src.clone();
	Mat threshold_output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	thresh =250;
	//do
	//{
		/// Detect edges using Threshold
		//threshold( src_gray, threshold_output, thresh, 255, THRESH_BINARY ); //How about go from top to bottom for thresh??
		//imshow("thres",threshold_output);
		/// Find contours
		findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

	//	thresh --;
	//} while(contours.size() != 1 && thresh > 150);
	cout<<"\nthresh:"<<thresh;
	
	/// Find the convex hull object for each contour
	vector<vector<Vec4i> > defects( contours.size());
	vector<vector<Point> >hull( contours.size() );
	vector<vector<int> > hullsI(contours.size());
	
	for( int i = 0; i < contours.size(); i++ )
	{   
		convexHull( Mat(contours[i]), hull[i], false );
		convexHull( Mat(contours[i]), hullsI[i], false );
		if(hullsI[i].size() > 2 )
		{
			try{
				convexityDefects(contours[i], hullsI[i], defects[i]);
	    	}
	    	catch(cv::Exception& e){
	    		//thresh++;
	    		//max_thresh = thresh;
	    		return;
	    	}

	    }
	}

	//Find top_most_point in ConvexHull	
	int min_y = 1000000;
	Point top_most_point;
	int index;

	for(int k = 0; k < hull[0].size();k++){
		if(hull[0][k].y < min_y){
			min_y = hull[0][k].y;
			top_most_point = hull[0][k];
			index = k;
		}
	}
	vector<vector<Point> > req_contour = contours;
	vector<vector<Point> > req_contour_temp = contours;
	vector<Point> temp_poly;
	/*req_polygon.push_back(hull[0][index-4]);
	req_polygon.push_back(hull[0][index]);
	req_polygon.push_back(hull[0][index+4]);
	*/
	for(int i=0;i<req_contour[0].size();i++){
		if(req_contour[0][i].y > (top_most_point.y + 150) ){
			req_contour[0].erase(req_contour[0].begin() + i);
		}
		else{
			temp_poly.push_back(req_contour[0][i]);
			//req_contour[0][i].x+=250;

		}
	}
	/*vector<Point> temp1(1);
	req_contour.push_back(temp1);
	int abc1 = 1, up=0, down=0;
	while(1){
		down = index-abc1;
		if(down >= 0){
			if(contours[0][down].y > (top_most_point.y + 150) )
				break;
			else{
				req_contour[0].push_back(contours[0][down]);
			}
		}
		abc1++;
	}
	abc1 = 1;
	req_contour[0].push_back(top_most_point);
	while(1){
		up = index+abc1;
		if(up < contours[0].size()){
			if(contours[0][up].y > (top_most_point.y + 150) )
				break;
			else{
				req_contour[0].push_back(contours[0][up]);
			}
		}
		abc1++;
	}*/

	Mat drawing = Mat::zeros( threshold_output.size(), CV_8UC3 );
	
	const Point* p = &temp_poly[0];
	int n = (int)temp_poly.size();
	//circle(image, req_polygon[0], 5, colr);
	polylines(drawing, &p, &n, 1, true, Scalar(0,0,255), 3, CV_AA);
    
    //ConvexityDefects also gives the points of the finger tips along with the point of the defect and the depth
	float max_defect_depth = 0.0;
	int no_of_fingers = 1;
	int temp;

	for(int i=0; i<contours.size();i++){
    	for(int j=0;j<defects[i].size();j++){
    		temp = defects[i][j][0];
    		if((contours[i][temp].x == top_most_point.x) && (contours[i][temp].y == top_most_point.y)){
    			circle(drawing, top_most_point, 5, Scalar(0,255,0), -1, 8, 0);
    		}	
		}
	}

	for( int i = 0; i < defects[0].size(); i++)
	{
		if(defects[0][i][3] > max_defect_depth)
			max_defect_depth = defects[0][i][3];
		
		//Try something else them a static value of 10000
		if(defects[0][i][3] > 10000)
			no_of_fingers++;
	}
	cout<<"Max Depth: "<<max_defect_depth<<endl<<"No of Fingers: "<<no_of_fingers<<endl;

	//Display Hull Points
	//for(int i=0;i<hull[0].size();i++)
	//	cout<<"point:"<<i<<" "<<hull[0][i].x<<","<<hull[0][i].y<<endl;
	
	/// Draw contours + hull results
	
	for( int i = 0; i< req_contour.size(); i++ )
	{
		Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
		//drawContours( drawing, req_contour, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
		drawContours( drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
	}
	
	/// Show in a window
	imshow( "Hull demo", drawing );
}