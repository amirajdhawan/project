//Finger Counting using Contour detection
//With dynamic threshold checking

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

Mat src;
Mat src_gray;
int thresh = 40;
int max_thresh = 200;	//Check different values
RNG rng(12345);

/// Function header
//void thresh_callback(int, void* );
/** @function main */
void find_contour(int, void* );

int main( int argc, char** argv )
{
	/// Load source image and convert it to gray
	src = imread( argv[1], 1 );
	namedWindow( "gray", CV_WINDOW_AUTOSIZE );
	/// Convert image to gray and blur it
	cvtColor( src, src_gray, CV_BGR2GRAY );
	blur( src_gray, src_gray, Size(3,3) );
	imshow("gray", src_gray);
	/// Create Window
	char* source_window = "Source";
	namedWindow( source_window, CV_WINDOW_AUTOSIZE );
	imshow( source_window, src );
	namedWindow( "Hull demo", CV_WINDOW_AUTOSIZE );
	namedWindow( "thr", CV_WINDOW_AUTOSIZE );
	
	find_contour(0, 0);
	createTrackbar( " Threshold:", "Hull demo", &thresh, max_thresh, find_contour );
	find_contour(0, 0);
	waitKey(0);
	return(0);
}
/** @function thresh_callback */
void find_contour(int, void* )
{
	Mat src_copy = src.clone();
	Mat threshold_output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	thresh = 90;
	//do
	//{
		/// Detect edges using Threshold
		threshold( src_gray, threshold_output, thresh, 255, THRESH_BINARY ); //How about go from top to bottom for thresh??
			
		/// Find contours
		findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

	//	thresh ++;
	//} while(contours.size() != 1 && thresh < 190);
	
	imshow("thr", threshold_output);
	cout<<"Outside "<<contours.size()<<" T= "<<thresh<<endl;

	/// Find the convex hull object for each contour
	vector<vector<Vec4i> > defects( contours.size());
	vector<vector<Point> >hull( contours.size() );
	vector<vector<int> > hullsI(contours.size());
	
	for( int i = 0; i < contours.size(); i++ )
	{   
		convexHull( Mat(contours[i]), hull[i], false );
		convexHull( Mat(contours[i]), hullsI[i], false );
		if(hullsI[i].size() > 2 )
		{
			try{
				convexityDefects(contours[i], hullsI[i], defects[i]);
	    		cout<<"defects "<<defects[i].size()<<endl; 
	    	}
	    	catch(cv::Exception& e){
	    		thresh--;
	    		max_thresh = thresh;
	    		return;
	    	}
	    }
	}

	//ConvexityDefects also gives the points of the finger tips along with the point of the defect and the depth
	float max_defect_depth = 0.0;
	int no_of_fingers = 1;

	for( int i = 0; i < defects[0].size(); i++)
	{
		cout<<"Depth of "<<i<<" : "<<defects[0][i][3]<<endl;
		if(defects[0][i][3] > max_defect_depth)
			max_defect_depth = defects[0][i][3];
		
		//Try something else them a static value of 10000
		if(defects[0][i][3] > 10000)
			no_of_fingers++;
	}
	cout<<"Max Depth: "<<max_defect_depth<<endl<<"No of Fingers: "<<no_of_fingers<<endl;

	//Display Hull Points
	//for(int i=0;i<hull[0].size();i++)
	//	cout<<"point:"<<i<<" "<<hull[0][i].x<<","<<hull[0][i].y<<endl;
	
	/// Draw contours + hull results
	Mat drawing = Mat::zeros( threshold_output.size(), CV_8UC3 );
	
	for( int i = 0; i< contours.size(); i++ )
	{
		Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
		drawContours( drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
		drawContours( drawing, hull, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
	}
	
	/// Show in a window
	imshow( "Hull demo", drawing );
}