#ifndef OBJ_RECOG_HPP
#define OBJ_RECOG_HPP

#include "src/circle_detect.hpp"
#include "src/rnt_detect.hpp"
#include "src/serial_port.hpp"
#include "src/rand_pixel_gen.hpp"
#include "src/detect_color.hpp"
#include "src/stabalize_objects.hpp"
#include "src/hand_detect.hpp"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <stdio.h>
#include <SerialStream.h>

enum COLORS_SUPPORTED{
	RED_COLOR,
	GREEN_COLOR,
	BLUE_COLOR
} COLORS;

enum SHAPES_SUPPORTED{
	CIRCLE,
	RECTANGLE,
	TRIANGLE
} SHAPES;

#define no_of_random_points 30
#define enum colors_user { RED, GREEN, BLUE }

#endif