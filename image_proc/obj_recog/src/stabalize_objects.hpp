#ifndef STABALIZE_OBJECTS_HPP
#define STABALIZE_OBJECTS_HPP

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

int stabalize_circle(std::vector<cv::Vec3f>, std::vector<cv::Vec3f>);
int stabalize_rect(std::vector<std::vector<cv::Point> >, std::vector<std::vector<cv::Point> >);

#define DEVIATION_CIRCLE 4;
#define DEVIATION_RECT 15;

#endif

