#include "stabalize_objects.hpp"

using namespace std;
using namespace cv;

int stabalize_circle(vector<Vec3f> circles, vector<Vec3f> previous_circle){
	if(circles.size() != previous_circle.size()){
		//cout<<"\nearly exit";
		return 0;
	}
	int deviation_allowed = DEVIATION_CIRCLE;
	float diffx = 0, diffy = 0, diffr = 0;
	for (int i = 0; i < circles.size(); i++){
		diffx = abs(circles[i][0] - previous_circle[i][0]);
		diffy = abs(circles[i][1] - previous_circle[i][1]);
		diffr = abs(circles[i][2] - previous_circle[i][2]);
		if(diffx > deviation_allowed || diffy > deviation_allowed || diffr > deviation_allowed){
			//cout<<"\ndiffx: "<<diffx<<" diffy: "<<diffy<<" diffr: "<<diffr;
			return 0;
		}
	}
	//cout<<"\nreturn true";
	return 1;
}

int stabalize_rect( vector<vector<Point> > rects, 
					vector<vector<Point> > previous_rects){
						//vector<vector<Point> > triangles,	
						//vector<vector<Point> > previous_triangles){
	if(rects.size() != previous_rects.size())
		return 0;
	int deviation_allowed = DEVIATION_RECT;
	int diffx = 0, diffy = 0;
	for( int i = 0; i < rects.size(); i++){
		for( int j = 0; j < rects[i].size(); j++){
			diffx = abs(rects[i][j].x - previous_rects[i][j].x);
			diffy = abs(rects[i][j].y - previous_rects[i][j].y);
			if(diffx > deviation_allowed || diffy > deviation_allowed)
				return 0;
		}
	}
	//cout<<"\nreturn true";
	return 1;
}