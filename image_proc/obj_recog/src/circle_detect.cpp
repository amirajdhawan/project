//Circle Detection
#include "circle_detect.hpp"

using namespace cv;
using namespace std;

int detect_circle(const Mat& image, vector<Vec3f>& circles)
{
	Mat src_gray;

	if( !image.data )
		return -1;
	cvtColor( image, src_gray, CV_BGR2GRAY );
	GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );
	HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 100, 50, 0, 0 );
	return circles.size();
}

void draw_detected_circles(Mat& image, vector<Vec3f>& circles, vector<Scalar> colors)
{
	/*if(circles.size() == 0)
		return 0;
	*/
	for( size_t i = 0; i < circles.size(); i++ )
	{
		Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
		//cout << "center: " << circles[i][0] << "," << circles[i][1] << endl;
		int radius = cvRound(circles[i][2]);
		//cout<<"radius: "<<radius;
		// circle center
		circle( image, center, 3, Scalar(0,255,0), -1, 8, 0 );
		// circle outline
		circle( image, center, radius, colors[i], 3, 8, 0 );
	}
	//return 1;
}