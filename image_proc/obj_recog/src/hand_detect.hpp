#ifndef HAND_DETECT_CPP
#define HAND_DETECT_CPP

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

cv::Point detect_hand(const cv::Mat, int, cv::Mat&);

#endif
