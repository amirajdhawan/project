#ifndef RNT_DETECT_HPP
#define RNT_DETECT_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <math.h>
#include <string.h>

void display_rects_n_triangles( cv::Mat&, 
								const std::vector<std::vector<cv::Point> >&,
							   	const std::vector<std::vector<cv::Point> >&,
							   	std::vector<cv::Scalar>,
							   	std::vector<cv::Scalar> );

int find_rects_n_triangles( const cv::Mat& , std::vector<std::vector<cv::Point> >& , 
							std::vector<std::vector<cv::Point> >& );

#endif
