#ifndef DETECT_COLOR_CPP
#define DETECT_COLOR_CPP

#include <math.h>
#include <iostream>
#include <opencv2/highgui/highgui.hpp>

//int +++_COLOR = [REDChannel, GREENChannel, BLUEChannel]
int detect_color(cv::Mat& image, std::vector<cv::Point>& , cv::Scalar&);

#endif