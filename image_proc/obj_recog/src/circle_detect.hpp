#ifndef CIRCLE_DETECT_HPP
#define CIRCLE_DETECT_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <stdio.h>

int detect_circle(const cv::Mat&, std::vector<cv::Vec3f>&);

void draw_detected_circles(cv::Mat&, std::vector<cv::Vec3f>&, std::vector<cv::Scalar>);

#endif