#include "rand_pixel_gen.hpp"

using namespace cv;
using namespace std;

void rand_pixels_rect_triangle(vector<Point>& random_test_points, vector<Point> object, int no_of_rand_points ){
	
	int upper_bound = 0, lower_bound = 100000, right_bound = 0, left_bound = 100000;
 	
 	for(int i = 0; i < object.size(); i++){
 		if(object[i].x > right_bound)
 			right_bound = object[i].x;
 		if(object[i].x < left_bound)
 			left_bound = object[i].x;

 		if(object[i].y > upper_bound)
 			upper_bound = object[i].y;
 		if(object[i].y < lower_bound)
 			lower_bound = object[i].y;
 	}
 	
 	int x_range = abs(right_bound - left_bound);
	int y_range = abs(upper_bound - lower_bound);
	//cout<<"lower_bound: "<<lower_bound<<" upper_bound: "<<upper_bound<<" right_bound:"<<right_bound<<" left_bound: "<<left_bound<<endl;
 	//vector<Point> approx_curve;
 	Point random_point;
 	//approxPolyDP(object, approx_curve, )
 	int i = 0;
 	//rectangle(src, Point(left_bound, upper_bound), Point(right_bound, lower_bound), Scalar(0, 255, 0));
	
 	while(i < no_of_rand_points){
 	
 		random_point.x = rand() % x_range + left_bound;
 		random_point.y = rand() % y_range + lower_bound;
 		double test = pointPolygonTest(object, random_point, false);
 		if(test >= 0){
 			random_test_points.push_back(random_point);
 			i++;
 			//circle(src, random_point, 1, Scalar(255,0,0));
 		}
 	}
}

void rand_pixels_circle(vector<Point>& random_test_points, Point center, int radius, int no_of_rand_points){
	int upper_bound = 0, lower_bound = 0, right_bound = 0, left_bound = 0;

	upper_bound = center.y + int(radius);
	lower_bound = center.y - int(radius);
	right_bound = center.x + int(radius);
	left_bound  = center.x - int(radius);

	Point random_point;
	int x_range = abs(right_bound - left_bound);
	int y_range = abs(upper_bound - lower_bound);

	int r_square = pow(radius,2);
	
	//rectangle(src, Point(left_bound, upper_bound), Point(right_bound, lower_bound), Scalar(0, 255, 0));
	
	int i = 0;
	while(i < no_of_rand_points)
	{
		random_point.x = rand() % x_range + left_bound;
		random_point.y = rand() % y_range + lower_bound;
		int equation_circle = (pow((random_point.x - center.x), 2) + pow((random_point.y - center.y), 2));
		if(equation_circle < r_square){
			//circle(src, random_point, 1, Scalar(255,0,0));	
			i++;
			random_test_points.push_back(random_point);
		}
		
		//cout<<random_point.x<<","<<random_point.y<<endl;	
	}
}