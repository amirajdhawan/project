//Detect Color
#include "detect_color.hpp"

using namespace cv;
using namespace std;


/*
int RED_RANGE[]   = { 210, 40, 40 };
int GREEN_RANGE[] = { 80, 50, 80 };
int BLUE_RANGE[]  = { 40, 40, 210 };
*/
int detect_color( Mat& image, vector<Point>& points, Scalar& color)
{
	int size_pt = points.size();
	unsigned long partial_sums[] = {0,0,0};
	Point3_<uchar>* p;
	unsigned long max_value = 0;
	Scalar temp;
	int index = 10;

	for( int i = 0; i < size_pt; i++){
		p = image.ptr<Point3_<uchar> >(points[i].y,points[i].x);
		
		partial_sums[0] += p->x;
		partial_sums[1] += p->y;
		partial_sums[2] += p->z;
	}
	for( int j = 0; j < 3; j++){
		if(size_pt != 0)
			partial_sums[j] =(int) (partial_sums[j] / size_pt);
		if(max_value < partial_sums[j]){
			index = j;
			max_value = partial_sums[j];
		}
	}

	switch(index){
		case 0:
			temp = Scalar(partial_sums[0], 0, 0);
			break;

		case 1:
			temp = Scalar(0, partial_sums[1], 0);
			break;

		case 2:
			temp = Scalar(0, 0, partial_sums[2]);
			break;

		default:
			temp = Scalar(0,0,0);
			break;
	}

	color = temp;
	/*
	for(int i = 0; i < points.size(); i++)
	{
		for( int j = 0; j < 3; j++)
			test[j] = image.at<Vec3b> (points[i].x,points[i].y) [j]; 
		
		if(color_required == 0){
			if((test[0] > RED_RANGE[0]) && (test[1] < RED_RANGE[1]) && (test[2] < RED_RANGE[2]))
				counter++;
		}
		else{
			if(color_required == 1){
				if((test[0] < GREEN_RANGE[0]) && (test[1] > GREEN_RANGE[1]) && (test[2] < GREEN_RANGE[2]))
					counter++;
			}
			else{
				if(color_required == 2){
					if((test[0] < BLUE_RANGE[0]) && (test[1] < BLUE_RANGE[1]) && (test[2] > BLUE_RANGE[2]))
						counter++;
				}
			}
		}
	}

	per = (counter / points.size());
	if(per >= 0.90)
		return 1;
	else
		return 0;
	*/
}