#include "hand_detect.hpp"

using namespace std;
using namespace cv;

std::vector < std::vector < cv::Point > > contours;
std::vector < cv::Vec4i > hierarchy;
std::vector < cv::Vec4i > defects;
std::vector < cv::Point > hull;
std::vector < int >  hullsI;
std::vector < cv::Point > largest_contour;

cv::Mat src_copy;
cv::Mat src_gray, threshold_output;
RNG rng1(12345);

Point detect_hand(const Mat src, int threshold_value, Mat& output_frame)
{
	src_copy = src.clone();

	//imshow("source", src);

	cvtColor( src_copy, src_gray, CV_BGR2GRAY );
	blur( src_gray, src_gray, Size(3,3) );
	//imshow("gray",src_gray);
	//do{
	threshold( src_gray, threshold_output, threshold_value, 255, THRESH_OTSU );
		//adaptiveThreshold( src_gray, threshold_output, 255, ADAPTIVE_THRESH_GAUSSIAN_C,  THRESH_BINARY, 3, -1 );
	//	imshow("thres",threshold_output);
			
	findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
	//	thresh++;
	//}while(thresh < 200);

	//cout<<"\nthresh:"<<thresh;
	if(contours.size() == 0)
		return Point(0,0);
	int max_area = 0;
	int index = 0;
	double current_area;

	for(int i = 0; i < contours.size(); i++){
		
		current_area = contourArea(contours[i]);
		if(current_area > max_area){
			max_area = current_area;
		 	index = i;
		}
	}
	
	largest_contour = contours[index];
	//free(contours);
	
	convexHull( Mat(largest_contour), hull, false );
	convexHull( Mat(largest_contour), hullsI, false );
	
	if(hullsI.size() > 2 && largest_contour.size() > 2 )
	{
		try{
			convexityDefects(largest_contour, hullsI, defects);
		}
		catch(cv::Exception& e){
			//thresh--;
			//max_thresh = thresh;
		}
	}

	int min_y = 1000000;
	Point top_most_point;
	index = 0;

	for(int k = 0; k < hull.size();k++){
		if(hull[k].y < min_y){
			min_y = hull[k].y;
			top_most_point = hull[k];
			index = k;
		}
	}

	//Mat drawing = Mat::zeros( threshold_output.size(), CV_8UC3 );
		
	const Point* p = &largest_contour[0];
	int n = (int)largest_contour.size();
	polylines(output_frame, &p, &n, 1, true, Scalar(0,0,255), 3, CV_AA);
	int temp = 0;

	for(int i = 0; i < defects.size(); i++){
		temp = defects[i][0];
		if((largest_contour[temp].x == top_most_point.x) && (largest_contour[temp].y == top_most_point.y))
    		circle(output_frame, top_most_point, 5, Scalar(0,255,0), -1, 8, 0);
	}

	Scalar color = Scalar( rng1.uniform(0, 255), rng1.uniform(0,255), rng1.uniform(0,255) );
	
	vector<vector<Point> > tempd;
	vector<vector<Point> > hulltemp;
	tempd.push_back(largest_contour);
	hulltemp.push_back(hull);

	drawContours( output_frame, tempd, 0, color, 1, 8, vector<Vec4i>(), 0, Point() );
	//drawContours( drawing, hulltemp, 0, color, 1, 8, vector<Vec4i>(), 0, Point() );	
	//imshow( "Hull demo", drawing );
	return top_most_point;
}