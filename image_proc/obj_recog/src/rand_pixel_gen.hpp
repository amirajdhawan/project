#ifndef RAND_PIXEL_GEN_HPP
#define RAND_PIXEL_GEN_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <math.h>

void rand_pixels_rect_triangle(std::vector<cv::Point>& , std::vector<cv::Point> , int);
void rand_pixels_circle(std::vector<cv::Point>&, cv::Point, int, int);

#endif
