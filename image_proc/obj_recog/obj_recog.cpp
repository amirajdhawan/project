#include "obj_recog.hpp"

using namespace cv;
using namespace std;

int check_in_circle(Vec3f, Point);
int check_in_polygon(vector<Point>, Point);
int check_finger_error(vector<Point> , Point , int);
Point get_avg_finger_tip(vector<Point>);
Point get_polygon_center(vector<Point>);
void feedback_system(Point , LibSerial::SerialStream&, VideoCapture&);
Point detect_arm(Mat&, Mat&);

int main(int argc, char** argv)
{
	/*if(argc < 3){
		cout<<"\nMinimum 2 arguments required\n";
		return -1;
	}
	*/
	//VideoCapture robot_cam(1);
	
	/*Input image from argument
	src = imread( argv[1], 1 );

	if( !src.data )
		{ return -1; }
	*/

	//Check if VideoCapture object is opened or not
	/*if(!robot_cam.isOpened() || !hand_detect.isOpened())
	{
		cout<<"\nUnable to connect to capture device"<<endl;
		return -1;
	}
	*/
	Mat obj_src_frame, hand_detect_frame, output_frame;
	namedWindow("Objects", CV_WINDOW_AUTOSIZE);
	//namedWindow("testing", CV_WINDOW_AUTOSIZE);
	
	//String of the serial port address
	const char* serial_name = "/dev/ttyACM0" ;

	//Object of SerialStream
	LibSerial::SerialStream serial_port;
	unsigned char cur_data = 0;

	int flow_control_circle = -1;
	int counter = 0, frame_req = 20, obj_type = -1, o0j_index = -1, anti_counter = 5, prev_counter = 0, miss_fingers = 3;

	vector<vector<Point> > rects;
	vector<vector<Point> > previous_rects;
	vector<Scalar> rects_colors;

	vector<vector<Point> > triangles;
	vector<vector<Point> > previous_triangles;
	vector<Point> random_test_points;
	vector<Point> random_test_points_tri;
	vector<Scalar> triangles_colors;

	vector<Vec3f> circles;
	vector<Vec3f> previous_circle;
	vector<Scalar> circle_colors;
	
	Point avg_finger_tip;
	Point finger_tip;
	Point previous_ft;
	vector<Point> finger;

	VideoCapture robot_cam(1);
	robot_cam.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	robot_cam.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if(!robot_cam.isOpened()){
		cout<<"\nUnable to connect to hand detect capture device"<<endl;
		return -1;
	}

	VideoCapture hand_detect_cam(0);
	hand_detect_cam.set(CV_CAP_PROP_FRAME_WIDTH, 320);
	hand_detect_cam.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
	if(!hand_detect_cam.isOpened()){
		cout<<"\nUnable to connect to hand detect capture device"<<endl;
		return -1;
	}

	if( !(setup_serial_port(  serial_port,
						serial_name,
						LibSerial::SerialStreamBuf::BAUD_9600)) ){
		cout<<endl<<"Serial Port connection failed";
		return -1;
	}
	while(1){
		//Capture a frame from the VideoCapture object
		robot_cam >> obj_src_frame;
		flip(obj_src_frame, obj_src_frame, 1);
		/*mcam >> obj_src_frame;
		cam.release();
		//cam->VideoCapture::~VideoCapture();

		//cam.open(1);
		if(!cap.isOpened()){
				cout<<"\nUnable to connect to capture device"<<endl;
		ma		return -1;
		}
		cap >> hand_detect_frame;
		cap.release();
		*/
		hand_detect_cam >> hand_detect_frame;
		flip(hand_detect_frame, hand_detect_frame, 1);
		output_frame = obj_src_frame;

		flow_control_circle++;
		//Check if the frame is empty or not
		if(obj_src_frame.empty()){
			cout<<"\nUnable to get image from object capture device"<<endl;
			return -1;
		}
		if(hand_detect_frame.empty()){
			cout<<"\nUnable to get image from Hand capture device"<<endl;
			return -1;
		}

		//Detect Circles
		if((flow_control_circle % 3) == 0){
			flow_control_circle = 0;
			if(detect_circle(obj_src_frame, circles) > 0 ){//Color Test
			/*for( int i = 0; i < circles.size(); i++){
				random_pixels_circle(random_test_points, Point(circles[i][0], circles[i][1]), (int) circles[i][2], no_of_random_points);
				cout<<"got random test points in circle\n";
				cout<<"Size: "<<circles.size()<<endl;
				//detect color and assign target_circle
				unsigned char temp = argv[2][0] - '0';
				int color_req = (int) temp;
				if(detect_color( obj_src_frame, random_test_points, color_req)){
					target_circle.push_back(circles[i]);
					cout<<"Target circle";
					draw_detected_circles(obj_src_frame, target_circle);
				}*/
			//draw_detected_circles(obj_src_frame, circles);
			/*for (int i = 0; i < circles.size(); i++){
				if( i < previous_circles.size()){
					if( check_circle_error(circles[i], previous_circle[i]) )
						circles = previous_circle;

					else
						previous_circle = circles;
				}
			}*/

				if(stabalize_circle(circles, previous_circle))
					circles = previous_circle;
				else
					previous_circle = circles;	
			}
			
			circle_colors.clear(); 
			for( int i = 0; i < circles.size(); i++){
				rand_pixels_circle(random_test_points, Point(circles[i][0], circles[i][1]), (int) circles[i][2], no_of_random_points);
				Scalar temp = Scalar(0,0,0);
				
				if(detect_color( obj_src_frame, random_test_points, temp)){
					circle_colors.push_back(temp);
				}
				random_test_points.clear();
			}
			
			//cout<<"\ncircle_colors: "<<circle_colors.size();			
		}
		draw_detected_circles(output_frame, circles, circle_colors);

		//Detect Rectangles & Triangles
		if(find_rects_n_triangles(obj_src_frame, rects, triangles)){
			if(stabalize_rect(rects, previous_rects))
				rects = previous_rects;
			else
				previous_rects = rects;

			if(stabalize_rect(triangles, previous_triangles))
				triangles = previous_triangles;
			else
				previous_triangles = triangles;


			//display_rects_n_triangles(obj_src_frame, rects, triangles);
			//Color Test
			for( int i = 0; i < rects.size(); i++){
				rand_pixels_rect_triangle(random_test_points, rects[i] , no_of_random_points);
				Scalar temp = Scalar(0,0,0);
				
				if(detect_color( obj_src_frame, random_test_points, temp)){
					rects_colors.push_back(temp);
				}
				random_test_points.clear();
				//rand_pixels_rect_triangle(random_test_points, rects[i], no_of_random_points);
				//cout<<"got random test points in rect\n";
			}
				
			for( int i = 0; i < triangles.size(); i++){
				//rand_pixels_rect_triangle(random_test_points_tri, triangles[i], no_of_random_points);
				rand_pixels_rect_triangle(random_test_points_tri, triangles[i] , no_of_random_points);
				//cout<<"got random test points in triangles\n";
				Scalar temp = Scalar(0,0,0);
				
				if(detect_color( obj_src_frame, random_test_points, temp)){
					triangles_colors.push_back(temp);
				}
				random_test_points.clear();
			}
			display_rects_n_triangles(output_frame, rects, triangles, rects_colors, triangles_colors);	
		}

		//Hand Detect
		finger_tip = detect_hand(hand_detect_frame, 130, output_frame);
		
		if(finger_tip.x > 320)
			finger_tip.x = 320;
		if(finger_tip.x < 0)
			finger_tip.x = 0;

		if(finger_tip.y > 320)
			finger_tip.y = 320;
		if(finger_tip.y < 0)
			finger_tip.y = 0;
		
		if(finger.size() == 0){
			finger.push_back(finger_tip);
		}

		if(finger_tip.x != 0 || finger_tip.y != 0)
			previous_ft = finger_tip;
		else
			finger_tip = previous_ft;

		if(check_finger_error(finger, finger_tip, 15) == 1){
			finger.push_back(finger_tip);
			anti_counter = miss_fingers;
			counter++;
		}

		else{
			if(anti_counter == 0){
				anti_counter = miss_fingers;
				counter = 0;
				finger.clear();
				prev_counter = 0;
			}
			if(anti_counter == miss_fingers){
				prev_counter = counter;
				anti_counter--;
			}
			else{
				if(prev_counter == counter)
					anti_counter--;
				else{
					anti_counter = miss_fingers;
					counter = 0;
					finger.clear();
				}
			}
		}
		if(counter <= (frame_req + 20) && counter >= frame_req){
			prev_counter = 0;
			counter = 0;
			anti_counter = miss_fingers;
			avg_finger_tip = get_avg_finger_tip(finger);
			finger.clear();
			
			for(int i = 0; i < circles.size(); i++){
				if(check_in_circle(circles[i], avg_finger_tip) == 1){
					circle(output_frame, Point(circles[i][0],circles[i][1]), circles[i][2], Scalar(0,0,255), -1);
					obj_type = 0;
					cout<<"x: "<<circles[i][0]<<" y: "<<circles[i][1]<<endl;
					feedback_system(Point(circles[i][0],circles[i][1]), serial_port, robot_cam);
					int flow_temp = 0;
					while(flow_temp < 100){
						robot_cam >> obj_src_frame;
						resize(obj_src_frame, obj_src_frame, Size(0,0),2, 2);
						flip(obj_src_frame, obj_src_frame, 1);
						imshow("Objects", obj_src_frame);
						waitKey(100);
						flow_temp++;
					}
					//waitKey(10000);
					break;		
				}
			}
			if(obj_type == -1){
				for(int i = 0; i < rects.size(); i++){
					if(check_in_polygon(rects[i],avg_finger_tip) == 1){
						obj_type = 1;
						const Point* p = &rects[i][0];
						int n = (int) rects[i].size();
						fillPoly(output_frame, &p, &n, 1, Scalar(0,0,255));
						Point polygon_center = get_polygon_center(rects[i]);
						circle(output_frame, polygon_center, 5, Scalar(0,255,0), -1);
						feedback_system(polygon_center, serial_port, robot_cam);
						waitKey(10000);
						break;
					}
				}
			}
			if(obj_type == -1){
				for(int i = 0; i < triangles.size(); i++){
					if(check_in_polygon(triangles[i], avg_finger_tip) == 1){
						obj_type = 2;
						const Point* p = &triangles[i][0];
						int n = (int) triangles[i].size();
						fillPoly(output_frame, &p, &n, 1, Scalar(0,0,255));
						Point polygon_center = get_polygon_center(rects[i]);
						feedback_system(polygon_center, serial_port, robot_cam);
						waitKey(10000);
						break;
					}	
				}
			}
			obj_type = -1;
		}

		cout<<endl<<counter;
		cout<<endl<<"finger x: "<<finger_tip.x<<" y: "<<finger_tip.y;

		
		/*if(counter == 0){
			for(int i = 0; i < circles.size(); i++){
				if(check_in_circle(circles[i], finger_tip) == 1){
					obj_type = 0;
					obj_index = i;		
				}
			}
			if(obj_type == -1 && obj_index == -1){
				for(int i = 0; i < rects.size(); i++){
					if(check_in_polygon(rects[i],finger_tip) == 1){
						obj_type = 1;
						obj_index = i;
					}
				}

				if(obj_type == -1 && obj_index == -1){
					for(int i = 0; i < triangles.size(); i++){
						if(check_in_polygon(triangles[i], finger_tip)){
							obj_index = i;
							obj_type = 2;
						}	
					}
				}
			}
			if(obj_type != -1 && obj_index != -1)
				counter++;
		}
		else {
			if(counter <= (frame_req + 20) && counter >= frame_req){
				//feedback call
				const Point* p;
				int n;
				switch(obj_type){
					case 0:
						circle(output_frame, Point (circles[obj_index][0], circles[obj_index][1]), circles[obj_index][2], Scalar(0,0,255),-1);
						break;

					case 1:
						p = &rects[obj_index][0];
						n = (int) rects[obj_index].size();
						fillPoly(output_frame, &p, &n, 1, Scalar(0,0,255));
						break;

					default:
						cout<<"jhol";
						break;
				}
			}
			else {
				switch(obj_type){
					case 0:
						if(obj_index < circles.size()){
							if(!check_in_circle(circles[obj_index], finger_tip))
								counter = 0;
						}
						counter++;
						break;

					case 1:
						if(obj_index < rects.size()){
							if(!check_in_polygon(rects[obj_index], finger_tip))
								counter = 0;
						}
						counter++;
						break;

					case 2:
						if(obj_index < triangles.size()){
							if(!check_in_polygon(triangles[obj_index], finger_tip))
								counter = 0;
						}
						counter++;
						break;

					default:
						counter = 0;
						break;
				}
			}
		}*/

		resize(output_frame, output_frame, Size(0,0), 2, 2);
		imshow("Objects", output_frame);
		waitKey(200);
		//cout<<endl<<"in";
	}
	waitKey(0);
	//Release Capture object
	//robot_cam.release();
	//hand_detect.release();
	robot_cam.release();
	hand_detect_cam.release();
	return 0;
}

int check_in_circle(Vec3f circle, Point temp){
	int r_square = pow(circle[2], 2);
	int equation_circle = (pow((temp.x - circle[0]), 2) + pow((temp.y - circle[1]), 2));
	if(equation_circle < r_square){
		return 1;
	}
	return 0;
}

int check_in_polygon(vector<Point> object, Point temp){
	double test = pointPolygonTest(object, temp, false);
	if(test >= 0){
		return 1;
	}
	return 0;
}

int check_finger_error(vector<Point> fingers, Point finger_tip, int sensitivity){
	int diffx, diffy;
	for(int i = 0; i < fingers.size(); i++){
		diffx = abs(fingers[i].x - finger_tip.x);
		diffy = abs(fingers[i].y - finger_tip.y);
		if(diffx > sensitivity && diffy > sensitivity)
			return 0;
	}
	return 1;
}

Point get_avg_finger_tip(vector<Point> fingers){
	long x_tot = 0,y_tot = 0;
	for(int i = 0; i < fingers.size(); i++){
		x_tot += fingers[i].x;
		y_tot += fingers[i].y;
	}
	return Point(x_tot/fingers.size(), y_tot/fingers.size());
}

Point get_polygon_center(vector<Point> object){
	int upper_bound = 0, lower_bound = 100000, right_bound = 0, left_bound = 100000;
 	
 	for(int i = 0; i < object.size(); i++){
 		if(object[i].x > right_bound)
 			right_bound = object[i].x;
 		if(object[i].x < left_bound)
 			left_bound = object[i].x;

 		if(object[i].y > upper_bound)
 			upper_bound = object[i].y;
 		if(object[i].y < lower_bound)
 			lower_bound = object[i].y;
 	}

 	Point center;
 	center.x = left_bound + (abs(right_bound - left_bound)/2);
 	center.y = lower_bound + (abs(lower_bound - upper_bound)/2);

 	return center;
}

void feedback_system(Point obj_center, LibSerial::SerialStream& serial_port, VideoCapture& obj){
	int flow = 1;
	Mat curr_frame, thresh_img;
	int obj_sensitivity = 7, return_sense = 7, y_offset = 5;
	//cout<<"call yo feedback_system"<<endl;
	cout<<"fs x: "<<obj_center.x<<" y: "<<obj_center.y<<endl;
	waitKey(100);
	while(flow){
		if(write_byte(serial_port, (char)'s'))
			if(write_byte(serial_port, (char) 't'))
				flow = 0;
	}
	flow = 1;
	int flag = 0;
	while(true){
		obj >> curr_frame;
		flip(curr_frame, curr_frame, 1);
		resize(curr_frame, curr_frame, Size(0,0), 2, 2);
		imshow("Objects", curr_frame);
		waitKey(50);
		flag++;
		if(flag == 40)
			break;
	}
	int xdiff = 0, ydiff = 0;
	double error, prev_error = 500000000000.0;
	char xdata, ydata;
	obj >> curr_frame;
	waitKey(100);
	while(1){
		//cout<<"chkpoint 1"<<endl;
	
		obj >> curr_frame;
		//waitKey(100)
		if(curr_frame.empty()){
			cout<<" Couldn't get frame from obj cam";
			continue;
		}
		flip(curr_frame, curr_frame, 1);
		Point arm_center = detect_arm(curr_frame, thresh_img);
		//circle(thresh_img, arm_center, 3, Scalar(0,0,255), -1);
		//circle(thresh_img, obj_center, 3, Scalar(255,0,0), -1);
		circle(curr_frame, arm_center, 3, Scalar(0,0,255), -1);
		circle(curr_frame, obj_center, 10, Scalar(255,0,0), -1);
		xdiff = abs(arm_center.x - obj_center.x);
		ydiff = abs(arm_center.y - obj_center.y);
		error = sqrt(pow(xdiff,2) + pow(ydiff,2));
		cout<<"Error: "<<error<<endl;
		if(true){
			prev_error = error;
			if(arm_center.y > (obj_center.y + obj_sensitivity))
				write_byte(serial_port, 'a');
			else{
				if(arm_center.y < (obj_center.y - obj_sensitivity))
					write_byte(serial_port,'c');
				else
					write_byte(serial_port,'b');
			}
			//waitKey(20);
			if(arm_center.x > (obj_center.x + obj_sensitivity))
				write_byte(serial_port, 'a');
			else{
				if(arm_center.x < (obj_center.x - obj_sensitivity))
					write_byte(serial_port, 'c');
				else
					write_byte(serial_port, 'b');
			}
			waitKey(220);

			if(arm_center.x <= (obj_center.x + return_sense) && arm_center.x >= (obj_center.x - return_sense) && arm_center.y <= (obj_center.y + return_sense) && arm_center.y >= (obj_center.y - return_sense)){
				while(flow){
					if(write_byte(serial_port, (char)'d'))
						if(write_byte(serial_port, (char) 'e'))
							flow = 0;
				}
				//write_byte(serial_port, 'd');
				cout<<"returning!!!!!!"<<endl;
				waitKey(100);
				return;
			}
		}
		/*else{
			if(prev_error <= error){
				while(flow){
					if(write_byte(serial_port, (char)'d'))
						if(write_byte(serial_port, (char) 'e'))
							flow = 0;
				}
				//write_byte(serial_port, 'd');
				return;
			}
		}*/
		resize(curr_frame, curr_frame, Size(0,0), 2, 2);
		//imshow("testing", thresh_img);
		imshow("Objects",curr_frame);
			
		waitKey(300);
	}

	return;
}

Point detect_arm(Mat& image, Mat& img_Threshed){
	Mat image_HSV;
	Moments mom;

	cvtColor(image, image_HSV, CV_BGR2HSV);
	//cout<<"Color convert";
	//inRange(image_HSV, Scalar(150, 20, 0), Scalar(255, 120, 80), img_Threshed);
	inRange(image_HSV, Scalar(20, 80, 80), Scalar(30, 255, 255), img_Threshed);
	//cout<<"Color convert";
	mom = moments(img_Threshed, true);
	//imshow("testing", img_Threshed);
	int xcenter = 0, ycenter = 0;
	xcenter = mom.m10 / mom.m00;
	ycenter = mom.m01 / mom.m00;
	
	Point center;
	center.x = xcenter;
	center.y = ycenter;
	
	return center;
	//circle(img_Threshed, temp, 25, Scalar(155,155,255), 2, 8);

	//namedWindow("output");
	//namedWindow("input");

	//imshow("input", image);
	//imshow("output", img_Threshed);

}



















