# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/cmake-gui

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/amiraj/project/image_proc/obj_recog

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/amiraj/project/image_proc/obj_recog

# Include any dependencies generated for this target.
include CMakeFiles/obj_recog.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/obj_recog.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/obj_recog.dir/flags.make

CMakeFiles/obj_recog.dir/obj_recog.o: CMakeFiles/obj_recog.dir/flags.make
CMakeFiles/obj_recog.dir/obj_recog.o: obj_recog.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/obj_recog/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/obj_recog.dir/obj_recog.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obj_recog.dir/obj_recog.o -c /home/amiraj/project/image_proc/obj_recog/obj_recog.cpp

CMakeFiles/obj_recog.dir/obj_recog.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obj_recog.dir/obj_recog.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/obj_recog/obj_recog.cpp > CMakeFiles/obj_recog.dir/obj_recog.i

CMakeFiles/obj_recog.dir/obj_recog.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obj_recog.dir/obj_recog.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/obj_recog/obj_recog.cpp -o CMakeFiles/obj_recog.dir/obj_recog.s

CMakeFiles/obj_recog.dir/obj_recog.o.requires:
.PHONY : CMakeFiles/obj_recog.dir/obj_recog.o.requires

CMakeFiles/obj_recog.dir/obj_recog.o.provides: CMakeFiles/obj_recog.dir/obj_recog.o.requires
	$(MAKE) -f CMakeFiles/obj_recog.dir/build.make CMakeFiles/obj_recog.dir/obj_recog.o.provides.build
.PHONY : CMakeFiles/obj_recog.dir/obj_recog.o.provides

CMakeFiles/obj_recog.dir/obj_recog.o.provides.build: CMakeFiles/obj_recog.dir/obj_recog.o

CMakeFiles/obj_recog.dir/src/circle_detect.o: CMakeFiles/obj_recog.dir/flags.make
CMakeFiles/obj_recog.dir/src/circle_detect.o: src/circle_detect.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/obj_recog/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/obj_recog.dir/src/circle_detect.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obj_recog.dir/src/circle_detect.o -c /home/amiraj/project/image_proc/obj_recog/src/circle_detect.cpp

CMakeFiles/obj_recog.dir/src/circle_detect.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obj_recog.dir/src/circle_detect.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/obj_recog/src/circle_detect.cpp > CMakeFiles/obj_recog.dir/src/circle_detect.i

CMakeFiles/obj_recog.dir/src/circle_detect.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obj_recog.dir/src/circle_detect.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/obj_recog/src/circle_detect.cpp -o CMakeFiles/obj_recog.dir/src/circle_detect.s

CMakeFiles/obj_recog.dir/src/circle_detect.o.requires:
.PHONY : CMakeFiles/obj_recog.dir/src/circle_detect.o.requires

CMakeFiles/obj_recog.dir/src/circle_detect.o.provides: CMakeFiles/obj_recog.dir/src/circle_detect.o.requires
	$(MAKE) -f CMakeFiles/obj_recog.dir/build.make CMakeFiles/obj_recog.dir/src/circle_detect.o.provides.build
.PHONY : CMakeFiles/obj_recog.dir/src/circle_detect.o.provides

CMakeFiles/obj_recog.dir/src/circle_detect.o.provides.build: CMakeFiles/obj_recog.dir/src/circle_detect.o

CMakeFiles/obj_recog.dir/src/hand_detect.o: CMakeFiles/obj_recog.dir/flags.make
CMakeFiles/obj_recog.dir/src/hand_detect.o: src/hand_detect.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/obj_recog/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/obj_recog.dir/src/hand_detect.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obj_recog.dir/src/hand_detect.o -c /home/amiraj/project/image_proc/obj_recog/src/hand_detect.cpp

CMakeFiles/obj_recog.dir/src/hand_detect.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obj_recog.dir/src/hand_detect.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/obj_recog/src/hand_detect.cpp > CMakeFiles/obj_recog.dir/src/hand_detect.i

CMakeFiles/obj_recog.dir/src/hand_detect.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obj_recog.dir/src/hand_detect.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/obj_recog/src/hand_detect.cpp -o CMakeFiles/obj_recog.dir/src/hand_detect.s

CMakeFiles/obj_recog.dir/src/hand_detect.o.requires:
.PHONY : CMakeFiles/obj_recog.dir/src/hand_detect.o.requires

CMakeFiles/obj_recog.dir/src/hand_detect.o.provides: CMakeFiles/obj_recog.dir/src/hand_detect.o.requires
	$(MAKE) -f CMakeFiles/obj_recog.dir/build.make CMakeFiles/obj_recog.dir/src/hand_detect.o.provides.build
.PHONY : CMakeFiles/obj_recog.dir/src/hand_detect.o.provides

CMakeFiles/obj_recog.dir/src/hand_detect.o.provides.build: CMakeFiles/obj_recog.dir/src/hand_detect.o

CMakeFiles/obj_recog.dir/src/serial_port.o: CMakeFiles/obj_recog.dir/flags.make
CMakeFiles/obj_recog.dir/src/serial_port.o: src/serial_port.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/obj_recog/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/obj_recog.dir/src/serial_port.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obj_recog.dir/src/serial_port.o -c /home/amiraj/project/image_proc/obj_recog/src/serial_port.cpp

CMakeFiles/obj_recog.dir/src/serial_port.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obj_recog.dir/src/serial_port.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/obj_recog/src/serial_port.cpp > CMakeFiles/obj_recog.dir/src/serial_port.i

CMakeFiles/obj_recog.dir/src/serial_port.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obj_recog.dir/src/serial_port.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/obj_recog/src/serial_port.cpp -o CMakeFiles/obj_recog.dir/src/serial_port.s

CMakeFiles/obj_recog.dir/src/serial_port.o.requires:
.PHONY : CMakeFiles/obj_recog.dir/src/serial_port.o.requires

CMakeFiles/obj_recog.dir/src/serial_port.o.provides: CMakeFiles/obj_recog.dir/src/serial_port.o.requires
	$(MAKE) -f CMakeFiles/obj_recog.dir/build.make CMakeFiles/obj_recog.dir/src/serial_port.o.provides.build
.PHONY : CMakeFiles/obj_recog.dir/src/serial_port.o.provides

CMakeFiles/obj_recog.dir/src/serial_port.o.provides.build: CMakeFiles/obj_recog.dir/src/serial_port.o

CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o: CMakeFiles/obj_recog.dir/flags.make
CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o: src/rand_pixel_gen.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/obj_recog/CMakeFiles $(CMAKE_PROGRESS_5)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o -c /home/amiraj/project/image_proc/obj_recog/src/rand_pixel_gen.cpp

CMakeFiles/obj_recog.dir/src/rand_pixel_gen.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obj_recog.dir/src/rand_pixel_gen.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/obj_recog/src/rand_pixel_gen.cpp > CMakeFiles/obj_recog.dir/src/rand_pixel_gen.i

CMakeFiles/obj_recog.dir/src/rand_pixel_gen.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obj_recog.dir/src/rand_pixel_gen.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/obj_recog/src/rand_pixel_gen.cpp -o CMakeFiles/obj_recog.dir/src/rand_pixel_gen.s

CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o.requires:
.PHONY : CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o.requires

CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o.provides: CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o.requires
	$(MAKE) -f CMakeFiles/obj_recog.dir/build.make CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o.provides.build
.PHONY : CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o.provides

CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o.provides.build: CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o

CMakeFiles/obj_recog.dir/src/stabalize_objects.o: CMakeFiles/obj_recog.dir/flags.make
CMakeFiles/obj_recog.dir/src/stabalize_objects.o: src/stabalize_objects.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/obj_recog/CMakeFiles $(CMAKE_PROGRESS_6)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/obj_recog.dir/src/stabalize_objects.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obj_recog.dir/src/stabalize_objects.o -c /home/amiraj/project/image_proc/obj_recog/src/stabalize_objects.cpp

CMakeFiles/obj_recog.dir/src/stabalize_objects.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obj_recog.dir/src/stabalize_objects.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/obj_recog/src/stabalize_objects.cpp > CMakeFiles/obj_recog.dir/src/stabalize_objects.i

CMakeFiles/obj_recog.dir/src/stabalize_objects.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obj_recog.dir/src/stabalize_objects.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/obj_recog/src/stabalize_objects.cpp -o CMakeFiles/obj_recog.dir/src/stabalize_objects.s

CMakeFiles/obj_recog.dir/src/stabalize_objects.o.requires:
.PHONY : CMakeFiles/obj_recog.dir/src/stabalize_objects.o.requires

CMakeFiles/obj_recog.dir/src/stabalize_objects.o.provides: CMakeFiles/obj_recog.dir/src/stabalize_objects.o.requires
	$(MAKE) -f CMakeFiles/obj_recog.dir/build.make CMakeFiles/obj_recog.dir/src/stabalize_objects.o.provides.build
.PHONY : CMakeFiles/obj_recog.dir/src/stabalize_objects.o.provides

CMakeFiles/obj_recog.dir/src/stabalize_objects.o.provides.build: CMakeFiles/obj_recog.dir/src/stabalize_objects.o

CMakeFiles/obj_recog.dir/src/rnt_detect.o: CMakeFiles/obj_recog.dir/flags.make
CMakeFiles/obj_recog.dir/src/rnt_detect.o: src/rnt_detect.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/obj_recog/CMakeFiles $(CMAKE_PROGRESS_7)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/obj_recog.dir/src/rnt_detect.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obj_recog.dir/src/rnt_detect.o -c /home/amiraj/project/image_proc/obj_recog/src/rnt_detect.cpp

CMakeFiles/obj_recog.dir/src/rnt_detect.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obj_recog.dir/src/rnt_detect.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/obj_recog/src/rnt_detect.cpp > CMakeFiles/obj_recog.dir/src/rnt_detect.i

CMakeFiles/obj_recog.dir/src/rnt_detect.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obj_recog.dir/src/rnt_detect.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/obj_recog/src/rnt_detect.cpp -o CMakeFiles/obj_recog.dir/src/rnt_detect.s

CMakeFiles/obj_recog.dir/src/rnt_detect.o.requires:
.PHONY : CMakeFiles/obj_recog.dir/src/rnt_detect.o.requires

CMakeFiles/obj_recog.dir/src/rnt_detect.o.provides: CMakeFiles/obj_recog.dir/src/rnt_detect.o.requires
	$(MAKE) -f CMakeFiles/obj_recog.dir/build.make CMakeFiles/obj_recog.dir/src/rnt_detect.o.provides.build
.PHONY : CMakeFiles/obj_recog.dir/src/rnt_detect.o.provides

CMakeFiles/obj_recog.dir/src/rnt_detect.o.provides.build: CMakeFiles/obj_recog.dir/src/rnt_detect.o

CMakeFiles/obj_recog.dir/src/detect_color.o: CMakeFiles/obj_recog.dir/flags.make
CMakeFiles/obj_recog.dir/src/detect_color.o: src/detect_color.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/amiraj/project/image_proc/obj_recog/CMakeFiles $(CMAKE_PROGRESS_8)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/obj_recog.dir/src/detect_color.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obj_recog.dir/src/detect_color.o -c /home/amiraj/project/image_proc/obj_recog/src/detect_color.cpp

CMakeFiles/obj_recog.dir/src/detect_color.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obj_recog.dir/src/detect_color.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/amiraj/project/image_proc/obj_recog/src/detect_color.cpp > CMakeFiles/obj_recog.dir/src/detect_color.i

CMakeFiles/obj_recog.dir/src/detect_color.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obj_recog.dir/src/detect_color.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/amiraj/project/image_proc/obj_recog/src/detect_color.cpp -o CMakeFiles/obj_recog.dir/src/detect_color.s

CMakeFiles/obj_recog.dir/src/detect_color.o.requires:
.PHONY : CMakeFiles/obj_recog.dir/src/detect_color.o.requires

CMakeFiles/obj_recog.dir/src/detect_color.o.provides: CMakeFiles/obj_recog.dir/src/detect_color.o.requires
	$(MAKE) -f CMakeFiles/obj_recog.dir/build.make CMakeFiles/obj_recog.dir/src/detect_color.o.provides.build
.PHONY : CMakeFiles/obj_recog.dir/src/detect_color.o.provides

CMakeFiles/obj_recog.dir/src/detect_color.o.provides.build: CMakeFiles/obj_recog.dir/src/detect_color.o

# Object files for target obj_recog
obj_recog_OBJECTS = \
"CMakeFiles/obj_recog.dir/obj_recog.o" \
"CMakeFiles/obj_recog.dir/src/circle_detect.o" \
"CMakeFiles/obj_recog.dir/src/hand_detect.o" \
"CMakeFiles/obj_recog.dir/src/serial_port.o" \
"CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o" \
"CMakeFiles/obj_recog.dir/src/stabalize_objects.o" \
"CMakeFiles/obj_recog.dir/src/rnt_detect.o" \
"CMakeFiles/obj_recog.dir/src/detect_color.o"

# External object files for target obj_recog
obj_recog_EXTERNAL_OBJECTS =

obj_recog: CMakeFiles/obj_recog.dir/obj_recog.o
obj_recog: CMakeFiles/obj_recog.dir/src/circle_detect.o
obj_recog: CMakeFiles/obj_recog.dir/src/hand_detect.o
obj_recog: CMakeFiles/obj_recog.dir/src/serial_port.o
obj_recog: CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o
obj_recog: CMakeFiles/obj_recog.dir/src/stabalize_objects.o
obj_recog: CMakeFiles/obj_recog.dir/src/rnt_detect.o
obj_recog: CMakeFiles/obj_recog.dir/src/detect_color.o
obj_recog: /usr/local/lib/libopencv_calib3d.so
obj_recog: /usr/local/lib/libopencv_contrib.so
obj_recog: /usr/local/lib/libopencv_core.so
obj_recog: /usr/local/lib/libopencv_features2d.so
obj_recog: /usr/local/lib/libopencv_flann.so
obj_recog: /usr/local/lib/libopencv_gpu.so
obj_recog: /usr/local/lib/libopencv_highgui.so
obj_recog: /usr/local/lib/libopencv_imgproc.so
obj_recog: /usr/local/lib/libopencv_legacy.so
obj_recog: /usr/local/lib/libopencv_ml.so
obj_recog: /usr/local/lib/libopencv_nonfree.so
obj_recog: /usr/local/lib/libopencv_objdetect.so
obj_recog: /usr/local/lib/libopencv_photo.so
obj_recog: /usr/local/lib/libopencv_stitching.so
obj_recog: /usr/local/lib/libopencv_ts.so
obj_recog: /usr/local/lib/libopencv_video.so
obj_recog: /usr/local/lib/libopencv_videostab.so
obj_recog: CMakeFiles/obj_recog.dir/build.make
obj_recog: CMakeFiles/obj_recog.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable obj_recog"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/obj_recog.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/obj_recog.dir/build: obj_recog
.PHONY : CMakeFiles/obj_recog.dir/build

CMakeFiles/obj_recog.dir/requires: CMakeFiles/obj_recog.dir/obj_recog.o.requires
CMakeFiles/obj_recog.dir/requires: CMakeFiles/obj_recog.dir/src/circle_detect.o.requires
CMakeFiles/obj_recog.dir/requires: CMakeFiles/obj_recog.dir/src/hand_detect.o.requires
CMakeFiles/obj_recog.dir/requires: CMakeFiles/obj_recog.dir/src/serial_port.o.requires
CMakeFiles/obj_recog.dir/requires: CMakeFiles/obj_recog.dir/src/rand_pixel_gen.o.requires
CMakeFiles/obj_recog.dir/requires: CMakeFiles/obj_recog.dir/src/stabalize_objects.o.requires
CMakeFiles/obj_recog.dir/requires: CMakeFiles/obj_recog.dir/src/rnt_detect.o.requires
CMakeFiles/obj_recog.dir/requires: CMakeFiles/obj_recog.dir/src/detect_color.o.requires
.PHONY : CMakeFiles/obj_recog.dir/requires

CMakeFiles/obj_recog.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/obj_recog.dir/cmake_clean.cmake
.PHONY : CMakeFiles/obj_recog.dir/clean

CMakeFiles/obj_recog.dir/depend:
	cd /home/amiraj/project/image_proc/obj_recog && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/amiraj/project/image_proc/obj_recog /home/amiraj/project/image_proc/obj_recog /home/amiraj/project/image_proc/obj_recog /home/amiraj/project/image_proc/obj_recog /home/amiraj/project/image_proc/obj_recog/CMakeFiles/obj_recog.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/obj_recog.dir/depend

