 #!/bin/sh
  echo "1 SoX Sound Exchange - Convert WAV to FLAC with 16000" 
  sox test.wav test.flac rate 16k
  echo "2 Submit to Google Voice Recognition"
  wget -q -U "Mozilla/5.0" --post-file test.flac --header="Content-Type: audio/x-flac; rate=16000" -O - "http://www.google.com/speech-api/v1/recognize?lang=de-de&client=chromium" > test.ret
  echo "3 SED Extract recognized text" 
  cat test.ret
  cat test.ret | sed 's/.*utterance":"//' | sed 's/","confidence.*//' > test.txt
  echo "4 Remove Temporary Files"
  rm test.flac
  #rm test.ret
  echo "5 Show Text "
  
  cat test.txt
