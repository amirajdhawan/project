#include <Servo.h>

#define BaseServo 9
#define ShoulderServo 4
#define ShoulderServo1 5
#define ElbowServo 10

Servo servos[4];

float radius_base = 33;

double degtorad(int degree){
  return (double) (degree * (3.14159 / 180) );
}

int radtodeg(double rads){
  return (int) (rads * (180 / 3.14159) );
}

void get_cartision(float& x, float& y, float radius, double angle){
  x = radius * cos(degtorad(angle));
  y = radius * sin(degtorad(angle));
}

int get_polar_servo_base(float x, float radius){
  float temp = 0.0;
  int index = 0;
  Serial.println("In Loop");
  for (int i = 0; i < 45; i++)
  {
    //delay(10);
    temp = radius * cos(degtorad(i));
    //Serial.print(temp);
    //Serial.print("  i = ");
    //Serial.println(i);
    if(abs((temp - x)) <= 0.2)
      return i; 
  }
  for (int i = 225; i < 360; i++)
  {
   temp = radius * cos(degtorad(i));
   if(abs((temp - x)) <= 0.2)
     return i; 
  }
  /*x = x / radius_base;
  if( x >= -1 && x <= 1)
    return (radtodeg(acos(x)));
  return 0;*/
}

int cvt_polar_servo_base(int degree){
  if(degree <= 45 && degree >= 0)
    return (45 - degree);
  else{
    if(degree <= 360 && degree >= 225)
      return (360 - degree + 45);
    else
      return 90;
  }  
}

/*int cvt_servo_base_polar(int degree){
  
}
void move_servo(int servo_no, int degree){
  servo[servo_no].write(degree);
}
*/

void setup() 
{ 
  Serial.begin(9600);
  servos[0].attach(BaseServo);
  servos[1].attach(ShoulderServo);
  servos[2].attach(ShoulderServo1);
  servos[3].attach(ElbowServo);
  Serial.println("Started");
} 

void loop() 
{ 
  Serial.print("Enter x = ");
  while(Serial.available() == 0);
  float newx = Serial.parseFloat();
  int temp = cvt_polar_servo_base(get_polar_servo_base(newx, radius_base));
  Serial.print("temp = ");
  Serial.println(temp);
  while(Serial.available() == 0);
  if(Serial.read() == 'y')
    servos[0].write(temp);
    
}
  /*
  Sweep Code
  for(pos = 0; pos < 180; pos += 1)  
  {                                  
    servos[0].write(pos);              
    delay(15);                       
  } 
  for(pos = 180; pos>=1; pos-=1)    
  {                                
    servos[0].write(pos);             
    delay(15);                      
  }*/
