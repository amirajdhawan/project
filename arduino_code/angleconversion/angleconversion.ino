// Controlling a servo position using a potentiometer (variable resistor) 
// by Michal Rinott <http://people.interaction-ivrea.it/m.rinott> 
#include <math.h>
#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
 
int potpin = 0;  // analog pin used to connect the potentiometer
int val;    // variable to read the value from the analog pin 
float oldx, oldy, newx, newy = 0;
int r = 33;
int newang = 0;

double degtorad(int deg){
  return deg*(3.14159/180);
}

int radtodeg(double rad){
  return rad*(180/3.14159);
}
void setup() 
{ 
  Serial.begin(9600);
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
  oldx = r*cos(degtorad(45));
  oldy = r*cos(degtorad(45));
 Serial.print("oldx=");
Serial.println(oldx); 
Serial.print("oldy=");
Serial.println(oldy); 
} 
 
void loop() 
{ 
  if(Serial.available() > 0)
  {
    newx = Serial.parseFloat();
  Serial.print("newx=");
  Serial.println(newx);
  newx=newx/r;
  if(newx > -1 && newx <1){
  Serial.println("in");
    newang = radtodeg(acos(newx)) + 45;
  } 
 delay(10); 
  Serial.print("New angle= ");
  delay(20);
  Serial.println(newang);
  while(Serial.available() == 0 );
  if(Serial.read() == 'y'){
    
  //val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023) 
  //val = map(val, 0, 1023, 0, 179);     // scale it to use it with the servo (value between 0 and 180) 
  myservo.write(newang);                  // sets the servo position according to the scaled value 
  delay(15);                           // waits for the servo to get there 
} }}
