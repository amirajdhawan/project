#include <Servo.h>

#define BaseServo 9
#define ElbowServo 10
#define L1 210
#define incr_val 4
#define EMagnet 2
#define tmplvl 90
#define tmpup  60
#define tmpobj 115
#define input_pin 4

Servo servos[2];
Servo temp;

int curx = 0,cury = 0;
int curmot1 = 135, curmot2 = 30, curmot3 = 60;
int flow = 0;
char x_data = 0;
char y_data = 0;

int radtodeg(double rads){
  return (int) (rads * (180 / 3.14159) );
}

void move_motor(int curmot1_d, int curmot2_d, int finalmot1_d,int finalmot2_d){
  /*int flow = 0, step_c = 1, mot1, mot2;
  if(curmot1_d > finalmot1_d)
    mot1 = 1;
  else{
    if(curmot1_d = finalmot1_d)
      mot1 = 0;
    else
      mot1 = -1;
  }
  if(curmot2_d > finalmot2_d)
    mot2 = 1;
  else{
    if(curmot2_d = finalmot2_d)
      mot2 = 0;
    else
      mot2 = -1;
  }
  
  while(flow < 2){
   if(mot1 == 1)
     curmot1_d += step_c;
   else{
     if(mot1 == -1)
       curmot1_d -=step_c;
     else
       curmot1_d = curmot1_d;
      servos[0].write(
    }
  }*/
  int diff_mot1, diff_mot2, step_c = 1;
  int flow = 0, x=0, y=0;
  
  while(flow != 2){
    diff_mot1 = curmot1_d - finalmot1_d;
    diff_mot2 = curmot2_d - finalmot2_d;
    
    if(diff_mot1 < 0)
      curmot1_d += step_c;
    else{
     if(diff_mot1 > 0)
      curmot1_d -= step_c;
     else
      if(x == 0){
        flow++;
        x = 1;
      }
      //curmot1_d = curmot1_d;
    }
    
    if(diff_mot2 < 0)
      curmot2_d += step_c;
    else{
     if(diff_mot2 > 0)
      curmot2_d -= step_c;
     else
       if(y == 0){
         flow++;
         y = 1;
       }
       //curmot1_d = curmot1_d;
    }
    servos[0].write(curmot1_d);
    servos[1].write(curmot2_d);
    delay(15); 
  }
}

void move_motor1(int curmot1_d, int finalmot1_d){
  int diff_mot1, step_c = 1;
  int flow = 0;
/*  Serial.print("In move motor curr: ");
  Serial.print(curmot1_d);
  Serial.print("  final: ");
  Serial.println(finalmot1_d);  
  */while(flow != 1){
    diff_mot1 = curmot1_d - finalmot1_d;
        
    if(diff_mot1 < 0)
      curmot1_d += step_c;
    else{
      if(diff_mot1 > 0)
        curmot1_d -= step_c;
      else
        flow++;
      //curmot1_d = curmot1_d;
    }
    temp.write(curmot1_d);  
    delay(60);
  }
}

void park_robo(){
  
  move_motor(curmot1, curmot2, 135, 30);
 /* servos[0].write(135);
  delay(50);
  servos[1].write(30);
  delay(50);
  */
  move_motor1(curmot3, 60);
  curmot1 = 135;
  curmot2 = 30;
  curmot3 = 60;
  
}
void moveinx(int x){
  int theta1 = radtodeg(acos((double)x/(double)(2*L1)));
  int theta2 = radtodeg(acos((2*pow(L1,2) - pow(x,2))/(2*pow(L1,2))));
  
  //move_motor(curmot1, curmot2, (90 + theta1), (180 - theta2));
  //curmot1 = 90 + theta1;
  //curmot2 = 180 - theta2;
  servos[0].write(90+theta1);
  delay(100);
  servos[1].write(180-theta2);
  delay(100);
  /*Serial.print("Did x with x= ");
  Serial.print(x);
  Serial.print(" Theta1: ");
  Serial.print(theta1);
  Serial.print(" Theta2: ");
  Serial.print(theta2);
  Serial.println("");*/
}

void movetoxy(int x, int y=0){
  //Only for X
  /*int theta = radtodeg(acos((2*pow(L1,2) - pow(x,2))/ (2*pow(L1,2))));
  int motor1 = 90 + ((180 - theta)/2);
  int motor2 = 180 - theta;
  
  servos[0].write(motor1);
  delay(100);
  servos[1].write(motor2);
  delay(100);*/
  
  //For X & Y
  double temp = sqrt(pow(x,2)+pow(y,2));
  int theta1 = radtodeg(acos((double)x/temp));
  int theta2 = radtodeg(acos((double)(pow(x,2)+pow(y,2))/(double)(2*L1*temp)));
  int motor1 = 45 + theta1 + theta2;
  
  //int motor2 = 180 - acos((2*pow(L1,2) - pow(x,2) - pow(y,2))/(2*pow(L1,2)));
  int motor2 = 180 - 2*theta2;
  move_motor(curmot1, curmot2, motor1, motor2);
  curmot1 = motor1;
  curmot2 = motor2;
/*  servos[0].write(motor1);
  delay(100);
  servos[1].write(motor2);
  delay(100);
  
  
  Serial.print("Did xy x= ");
  Serial.print(x);
  Serial.print(" & y= ");
  Serial.print(y);
  Serial.print(" Theta1: ");
  Serial.print(theta1);
  Serial.print(" Theta2: ");
  Serial.print(theta2);
  Serial.println("");
*/
  
}

void setup(){
  Serial.begin(9600);
  servos[0].attach(BaseServo);
  servos[1].attach(ElbowServo);
  temp.attach(7);
  pinMode(EMagnet,OUTPUT);
  pinMode(input_pin, INPUT);
  //temp.write(160);
  //delay(50);
  Serial.println("Started");
  park_robo();
  delay(1000);
}

void start_position(){
  move_motor1(curmot3, tmplvl);
  curmot3 = tmplvl;
//  temp.write(100);
//  delay(50);
  movetoxy(80, 150);
  curx = 80;
  cury = 150;
  /*servos[0].write(90);
  delay(25);
  servos[1].write(90);
  delay(25);
  */Serial.println("Start_position");
}

void set_xy(char x_data, char y_data){
  
  switch(x_data){
    case 'a':
      if( (curx + incr_val) <= 380)
        curx += incr_val;
      break;
    case 'b':
      break;
    case 'c':
      if( (curx - incr_val) >= 50)
        curx -=incr_val;
      break;
    default:
      break;
  }
  switch(y_data){
    case 'a':
      if( (cury + incr_val) <= 200)
        cury += incr_val;
      break;
    case 'b':
      break;
    case 'c':
      if( (cury - incr_val) >= 0)
        cury -=incr_val;
      break;
    default:
      break;
  }
  if(curx % 5 == 0){
    move_motor1(curmot3, curmot3 + 1);
    curmot3 = curmot3 + 1;
  }  
  movetoxy(curx,cury);
}
char temp1;
void loop(){
  if(Serial.available() > 0){
    temp1 = Serial.read();
    if(temp1 == 's'){
      while(Serial.available() <= 0);
      temp1 = Serial.read();
      if(temp1 == 't'){
        //temp.write(110);
        //delay(50);
        Serial.println("entered");
        start_position();
        delay(100);
        /*while(1){for( int i = 4; i < 40; i+=1){
          moveinx(i);
        }
        
        for(int i = 40; i > 4; i-=1){
          moveinx(i);
        }}*/
        flow = 1;
        while(flow){
          while(Serial.available() <= 0);
          x_data = Serial.read();
          while(Serial.available() <= 0);
          y_data = Serial.read();
          /*Serial.print("xdata= ");
          Serial.print(x_data);
          Serial.print(" ydata= ");
          Serial.println(y_data);
          */
          if(x_data == 'd' && y_data == 'e')
          {  
            flow = 0;
            //curx = curx - 5;
            //cury = cury - 5;
            //movetoxy(curx,cury);
            //Serial.println("pt1");
          }
          else
            set_xy(x_data, y_data);
          delay(100);
        }
      }
      delay(200);
      //movetoxy(curx, cury-3);
      //cury -= 3;
      digitalWrite(EMagnet,HIGH);
      move_motor1(curmot3, tmpobj);      
      curmot3 = tmpobj;
      delay(200);
      
      /*while(true){
        Serial.print("looping  ");
        Serial.println(curmot3);
        if(digitalRead(input_pin) == 0)
          break;
        else{
          if(curmot3 < 180){
            curmot3++;
            temp.write(curmot3);
            delay(100);
          }
          else
            break;
        }
      }*/
      Serial.println("at obj lvl");
      move_motor1(curmot3, tmpup);
      curmot3 = tmpup;
      delay(1000);
      Serial.println("at up lvl");
      
      move_motor1(curmot3, tmplvl);
      curmot3 = tmplvl;
      delay(100);
      Serial.println("at tmp lvl");
      
      movetoxy(390, 0);
      delay(100);

      move_motor1(curmot3, tmpobj);
      curmot3 = tmpobj;
      delay(100);
      Serial.println("at obj lvl");
      
      digitalWrite(EMagnet, LOW);
      delay(100);
      
      move_motor1(curmot3, tmplvl);
      curmot3 = tmplvl;
      delay(1000);
            Serial.println("at tmp lvl");
      
      move_motor1(curmot3, tmpup);
      curmot3 = tmpup;
      delay(50);
      
      /*for (int i = 100; i < 121; i++){
        temp.write(i);
        delay(25);
      }
      delay(200);
      for (int i = 120; i > 50; i--){
        temp.write(i);
        delay(25);
      }*/
      /*move_motor1(curmot3, 50);
      curmot3 = 50;
      
      Serial.println("reached");
      
      //start_position();
      for (int i = 110; i < 121; i++){
        temp.write(i);
        delay(25);
      }
      delay(2000);
      digitalWrite(EMagnet, LOW);
      for (int i = 50; i < 101; i++){
        temp.write(i);
        delay(25);
      }*/
      park_robo();
    }    
  }
  delay(500);
}
  /*if(flow == 0){
    temp.write(110);
    delay(500);
    flow=1;
  }
  for(reqx = 0; reqx < 20;reqx++){
    movetoxy(20,reqx);
    //moveinx(reqx);
    delay(10);
  }
  for(reqx=20;reqx>-1;reqx--){
    movetoxy(20,reqx);
    delay(10);
  }*/
  /*if(Serial.available() > 0){
    reqx = (int) Serial.parseInt();
    while(Serial.available() == 0);
    reqy = (int) Serial.parseInt();
    
    movetoxy(reqx, reqy);
  }*/
  

