void put(int servo, int angle)
{
  //servo is the servo number (typically 0-7)
  //angle is the absolute position from 500 to 5500
 
  unsigned char buff[4];
 
  unsigned char pos_hi,pos_low;
 
  //Convert the angle data into two 7-bit bytes
  pos_hi=angle & 0x7F;
  pos_low=(angle>>7) & 0x7f;
 
  //Construct a Pololu Protocol command sentence
  buff[0]=0x84; //start byte
  buff[1]=servo; //device id
//  buff[2]=0x04; //command number
  //buff[3]=servo; //servo number
  buff[2]=pos_hi; //data1
  buff[3]=pos_low; //data2
 
  //Send the command to the servo controller
  for(int i=0;i<4;i++){
    Serial3.write(buff[i]);
  }
}

void setup(){
  Serial3.begin(9600);
  Serial.begin(9600);
  Serial3.write(0xAA);
}

void loop(){
  put(0,100);
  delay(1000);
  put(0,15000);
  delay(1000);
}
